		</div>
	</div><!-- .site-main -->
	
	<?php do_action('p3_site_main_end'); ?>

	<div class="hide-back-to-top"><div id="back-top"><a href="#top"><i class="fa fa-chevron-up"></i></a></div></div>
	
	<div id="p3_sticky_stop"></div>

	<?php if ( is_active_sidebar( 'ad-area-2' ) ) { ?>
		<div id="ad-area-2" class="clearfix container textalign-center">
			<div class="col-xs-12">
				<?php dynamic_sidebar( 'ad-area-2' ); ?>
			</div>
		</div>
	<?php } // end if ?>
	
	<?php get_sidebar( 'footer' ); ?>
	
	<?php if (get_theme_mod('posts_carousel_footer')) { ?>
	<div class="carousel-footer">
		<h3><?php _e('Where to next?', 'pipdig-textdomain'); ?></h3>
		<?php get_template_part('inc/chunks/posts-carousel'); ?>
	</div>
	<?php } // end if ?>
	
	<?php if(get_theme_mod('social_count_footer') && function_exists('pipdig_p3_social_footer')) { ?>
		<?php pipdig_p3_social_footer();?>
	<?php } // end if ?>
	
	<?php do_action('p3_footer_bottom'); ?>
	
	<footer class="site-footer">
		<div class="clearfix container">
			<div class="row">
				<?php if (get_theme_mod('disable_responsive')) {
					$sm = $md = 'xs';
					} else {
						$sm = 'sm';
						$md = 'md';
					}
				?>
				<div class="col-<?php echo $sm; ?>-7 site-info">
				</div>
				
				<div class="col-<?php echo $sm; ?>-5 site-credit">
	               					<?php $copy = get_theme_mod('copyright_statement');
					if ($copy) {
						echo wp_kses_post($copy);
					} else { ?>
						&copy; <?php echo date('Y'); ?> <a href="<?php echo esc_url(home_url('/')); ?>"><?php echo bloginfo( 'name' ); ?></a>
					<?php } // end if ?>
					<?php if (has_nav_menu('footer')) {
						wp_nav_menu( array('container_class' => 'footer-menu-bar', 'theme_location' => 'footer'));
					} ?>
				</div>
			</div>
		</div>
	</footer>
	
<?php wp_footer(); ?>
</body>
</html>