<?php get_header(); ?>

	<div class="row">

			<?php if (is_product_category() || is_shop() || is_product_tag()){ ?>
				<div class="<?php pipdig_left_or_right(); ?> content-area">
			<?php } else { ?>
				<div class="col-xs-12 content-area">
			<?php } // end if ?>

			<?php woocommerce_content(); ?>

		</div><!-- .content-area -->

		<?php //Show sidebar? i.e. not on products
		if (is_product_category() || is_shop() || is_product_tag()){
			get_sidebar();
		}
		?>
	</div>

<?php get_footer(); ?>