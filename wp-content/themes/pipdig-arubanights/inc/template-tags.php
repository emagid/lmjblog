<?php
if (!function_exists('pipdig_content_nav')) {
	function pipdig_content_nav( $nav_id ) {
		global $wp_query, $post;

		// Don't print empty markup on single pages if there's nowhere to navigate.
		if ( is_single() ) {
			$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
			$next = get_adjacent_post( false, '', false );

			if ( ! $next && ! $previous )
				return;
		}

		// Don't print empty markup in archives if there's only one page.
		if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
			return;

		$nav_class = ( is_single() ) ? 'post-navigation' : 'paging-navigation';

		?>
		<nav id="<?php echo esc_attr( $nav_id ); ?>" class="clearfix <?php echo $nav_class; ?>">
		<?php if ( is_single() ) : ?>

			<?php
			/*
			$the_shape = absint(get_theme_mod('p3_related_posts_shape', 1));
			
			$shape = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAFoAQMAAAD9/NgSAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAADJJREFUeNrtwQENAAAAwiD7p3Z7DmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5HHoAAHnxtRqAAAAAElFTkSuQmCC'; // landscape
		
			if ($the_shape == 2) {
				
				$shape = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAAHgAQMAAACyyGUjAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAACxJREFUeNrtwTEBAAAAwiD7p7bGDmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkHVZAAAFam5MDAAAAAElFTkSuQmCC'; // portrait
				
			} elseif ($the_shape == 3) {
				$shape = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0AQMAAADxGE3JAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAADVJREFUeNrtwTEBAAAAwiD7p/ZZDGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOX0AAAEidG8rAAAAAElFTkSuQmCC'; // square
			}
			*/
			
			$nextPost = get_previous_post();
			
			if ($nextPost) {
				
				$nextthumbnail = get_the_post_thumbnail($nextPost->ID, 'thumbnail' ); 
				//$nextthumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $nextPost->ID ), 'thumbnail' ); // just image source
				
				if ($nextthumbnail) {
					previous_post_link( '<div class="nav-previous">%link</div>', $nextthumbnail.'<span class="meta-nav"><i class="fa fa-chevron-left"></i> ' . __( 'Previous Post', 'pipdig-textdomain' ) . '</span> %title' );
					//previous_post_link( '<div class="nav-previous">%link</div>', '<div class="p3_cover_me next_prev_post_thumb" style="background-image:url('.$nextthumbnail_src[0].');"><img src="'.$shape.'" alt="" class="p3_invisible" data-pin-nopin="true"/></div><span class="meta-nav"><i class="fa fa-chevron-left"></i> ' . __( 'Previous Post', 'pipdig-textdomain' ) . '</span> %title' );
				} else { // no thumbnail available
					previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav"><i class="fa fa-chevron-left"></i> ' . __( 'Previous Post', 'pipdig-textdomain' ) . '</span> %title' );
				}
				
			}
			
			
			$prevPost = get_next_post();
			
			if ($prevPost) {
				
				$prevthumbnail = get_the_post_thumbnail($prevPost->ID, 'thumbnail' ); 
				
				if ($prevthumbnail) {
					next_post_link( '<div class="nav-next">%link</div>', $prevthumbnail.'<span class="meta-nav">' . __( 'Next Post', 'pipdig-textdomain' ) . ' <i class="fa fa-chevron-right"></i></span> %title' );
				} else { // no thumbnail available
					next_post_link( '<div class="nav-next">%link</div>', '<span class="meta-nav">' . __( 'Next Post', 'pipdig-textdomain' ) . ' <i class="fa fa-chevron-right"></i></span> %title' );
				}
				
			}
			?>

		<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : ?>

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( '<span class="meta-nav"><i class="fa fa-chevron-left"></i></span> '.__( 'Older Posts', 'pipdig-textdomain' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer Posts', 'pipdig-textdomain' ).' <span class="meta-nav"><i class="fa fa-chevron-right"></i></span>' ); ?></div>
			<?php endif; ?>

		<?php endif; ?>

		</nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
		<?php
	}
}

if(!function_exists('pipdig_comment')) {
	function pipdig_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;

		if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) { ?>

			<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
				<div class="comment-body">
					Pingback: <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'pipdig-textdomain' ), '<span class="comment-meta"><span class="edit-link"><i class="fa fa-pencil"></i>', '</span></span>' ); ?>
				</div>

		<?php } else { ?>

			<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
				<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
					<div class="comment-meta">
						<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
						<?php printf('<div class="comment-author">%s</div>', sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
						<span class="comment-date"><?php printf( _x( '%1$s / %2$s', '1: date, 2: time', 'pipdig-textdomain' ), get_comment_date(), get_comment_time() ); ?></span>
					</div>

					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'pipdig-textdomain' ); ?></p>
					<?php endif; ?>

					<div class="comment-content">
						<?php comment_text(); ?>
					</div>

					<div class="comment-meta comment-footer">
						<?php edit_comment_link( __( 'Edit', 'pipdig-textdomain' ), '<span class="edit-link"><i class="fa fa-pencil"></i>', '</span>' ); ?>
						<?php
							comment_reply_link( array_merge( $args, array(
								'add_below' => 'div-comment',
								'depth'     => $depth,
								'max_depth' => $args['max_depth'],
								'before'    => '<span class="comment-reply"><i class="fa fa-reply"></i>',
								'after'     => '</span>',
							) ) );
						?>
					</div>
				<!-- #div-comment-<?php comment_ID(); ?> --></article>

		<?php
		}
	}
}
