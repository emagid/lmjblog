function pipdigResetFonts() {
	
	if (confirm("Warning! This will reset any fonts back to the theme defaults and save all settings. Is that ok?")) {
		var data = {
			wp_customize: 'on',
			action: 'pipdig_fonts_customizer_reset',
		};

		$.post(ajaxurl, data, function () {
			wp.customize.state('saved').set(true);
			location.reload();
		});
    }
	
}