<?php

if (!defined('ABSPATH')) die;

function pipdig_deactivate_ocdi() {
	if (!class_exists('OCDI_Plugin')) {
		return;
	}
	if ( (get_option('p3_demo_imported') == 1) && (get_option('p3_demo_imported_override') != 1) ) {
		deactivate_plugins('one-click-demo-import/one-click-demo-import.php');
		update_option('p3_demo_imported_override', 1);
	}
}
add_action('admin_init', 'pipdig_deactivate_ocdi');

// import config
function pipdig_oneclick_import_files() {
	$message = "We recommend that you only import the demo content if you are starting a brand new site. Are you sure?";
	return array(
		array(
			'import_file_name' => 'Demo Content',
			'import_file_url' => 'https://www.dropbox.com/s/g1d0cbx0ss11rlz/20%20posts.xml?dl=1',
			'import_widget_file_url' => 'https://www.dropbox.com/s/jeql8l3vxycvf0z/standard-widgets.wie?dl=1',
			//'import_customizer_file_url' => '',
			//'import_preview_image_url' => '',
			'import_notice'	=> $message,
			//'preview_url' => '',
		),
		/*
		array(
			'import_file_name' => 'Classic Blog',
			'import_file_url' => 'https://www.dropbox.com/s/acbnri07qw36t81/content.xml?dl=1',
			'import_widget_file_url' => 'https://www.dropbox.com/s/506ae2h43i2nqvn/widgets.wie?dl=1',
			'import_customizer_file_url' => 'https://www.dropbox.com/s/p8mhhdse8gadro3/customizer.dat?dl=1',
			'import_preview_image_url' => 'https://preview.pipdig.co/assets/wordpress/aruba-nights-1.jpg',
			'import_notice'	=> $message,
			'preview_url' => 'https://arubanights.pipdig.co/',
		),
		array(
			'import_file_name' => 'Pastel Mint',
			'import_file_url' => 'https://www.dropbox.com/s/i48k5bjw08euqli/content.xml?dl=1',
			'import_widget_file_url' => 'https://www.dropbox.com/s/uue0s6vucxi67aj/widgets.wie?dl=1',
			'import_customizer_file_url' => 'https://www.dropbox.com/s/jap4u0zksj6i7gq/customizer.dat?dl=1',
			'import_preview_image_url' => 'https://preview.pipdig.co/assets/wordpress/aruba-nights-2.jpg',
			'import_notice'	=> $message,
			'preview_url' => 'https://arubanights.pipdig.co/two',
		),
		array(
			'import_file_name' => 'Single Column',
			'import_file_url' => 'https://www.dropbox.com/s/mv6azk2lgbn4auc/content.xml?dl=1',
			'import_widget_file_url' => 'https://www.dropbox.com/s/29hkrkmgip7hn5s/widgets.wie?dl=1',
			'import_customizer_file_url' => 'https://www.dropbox.com/s/n6q9z1vkf5wv4bw/customizer.dat?dl=1',
			'import_preview_image_url' => 'https://preview.pipdig.co/assets/wordpress/aruba-nights-3.jpg',
			'import_notice'	=> $message,
			'preview_url' => 'https://arubanights.pipdig.co/single-column',
		),
		array(
			'import_file_name' => 'Grid Layout',
			'import_file_url' => 'https://www.dropbox.com/s/b3rt774mai32z4z/content.xml?dl=1',
			'import_widget_file_url' => 'https://www.dropbox.com/s/istyv36gut755mn/widgets.wie?dl=1',
			'import_customizer_file_url' => 'https://www.dropbox.com/s/6azuwwoas3sn9x6/customizer.dat?dl=1',
			'import_preview_image_url' => 'https://preview.pipdig.co/assets/wordpress/aruba-nights-4.jpg',
			'import_notice'	=> $message,
			'preview_url' => 'https://arubanights.pipdig.co/single-column',
		),
		*/
	);
}
add_filter( 'pt-ocdi/import_files', 'pipdig_oneclick_import_files' );

// set some options before import starts
function pipdig_ocdi_before_import() {
	
	// remove theme mods
	remove_theme_mods();
	
	// deactivate widgets (move them to inactive area)
	update_option('sidebars_widgets', array());

}
add_action( 'pt-ocdi/before_content_import', 'pipdig_ocdi_before_import' );

// set some options after import completes
function pipdig_ocdi_after_import_setup() {
	
	// remove current menus
	remove_theme_mod('nav_menu_locations');
	
	// set demo menus
	$primary_menu = get_term_by('name', 'Primary', 'nav_menu');
	set_theme_mod('nav_menu_locations', array(
			'primary' => $primary_menu->term_id,
		)
	);
	
	// set to post excerpt layout
	set_theme_mod('home_layout', 2);
	set_theme_mod('category_layout', 2);
	set_theme_mod('p3_pinterest_hover_image_file', 'https://pipdigz.co.uk/p3/img/pin/white_oswald.png');
	set_theme_mod('p3_pinterest_hover_image_position', 'center');
	
	set_theme_mod('pipdig_lazy', 1);
	
	// remove any homepage settings
	update_option('show_on_front', 'posts');
	delete_option('page_on_front');
	delete_option('page_for_posts');
	
	global $wp_rewrite;
	$wp_rewrite->set_permalink_structure('/%postname%/');
	flush_rewrite_rules();
	
	update_option('p3_auto_updates_on', 1);	update_option('p3_demo_imported', 1);
	update_option('p3_demo_imported_override', 0);

}
add_action( 'pt-ocdi/after_import', 'pipdig_ocdi_after_import_setup' );

// intro text
function ocdi_plugin_intro_text($default_text) {
	$default_text = '
	<div style="background: #fff; margin: 30px 0; padding: 5px 20px; border: 1px solid #ddd">
	<p>You can use the options below to import the content from our preview sites. <a href="https://go.pipdig.co/open.php?id=demo-content" target="_blank" rel="noopener">Click here</a> for more information.</p>
	</div>';
	return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'ocdi_plugin_intro_text' );

function pipdig_ocdi_plugin_page_setup($default_settings) {
	$default_settings['parent_slug'] = 'themes.php';
	$default_settings['page_title'] = 'Import Demo Content';
	$default_settings['menu_title'] = 'Import Demo Content';
	$default_settings['capability'] = 'import';
	$default_settings['menu_slug'] = 'pipdig-demo-import';
	return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'pipdig_ocdi_plugin_page_setup' );

// remove branding
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

// stop generation of thumbnails. quicker imports.
add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );