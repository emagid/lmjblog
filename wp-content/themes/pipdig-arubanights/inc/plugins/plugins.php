<?php if (!defined('ABSPATH')) die;

if (!is_admin() || p3_check_file()) {
	return;
}

require_once 'tgm.php';

function pipdig_plugins_install() {

    $plugins = array(
		array(
			'name' => 'Regenerate Thumbnails',
			'slug' => 'regenerate-thumbnails',
			'required' => true,
		),
		array(
			'name' => 'Meta Box',
			'slug' => 'meta-box',
			'required' => true,
		),
		array(
			'name' => 'pipdig Power Pack (p3)',
			'slug' => 'p3',
			'source' => 'http://wpupdateserver.com/p3.zip',
			'required' => true,
		),
    );
	
	if (!defined('PIPDIG_P3_V')) {
		$plugins[] = array(
			'name' => 'Image Settings',
			'slug' => 'imsanity',
			'required' => true,
		);
	}

	$config = array(
		'id' => 'pipdig-textdomain',
		'default_path' => '',
		'menu' => 'pipdig-install-plugins',
		'parent_slug' => 'themes.php',
		'capability' => 'edit_theme_options',
		'is_automatic' => true, // activate
		'strings' => array(
			'notice_can_install_required' => _n_noop(
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'pipdig-textdomain'
			),
		),
		
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'pipdig_plugins_install' );
