<?php
if (class_exists('WP_Customize_Control')) {
	class WP_Customize_Category_Control extends WP_Customize_Control {
		public function render_content() {
			$dropdown = wp_dropdown_categories(
				array(
					'name' => '_customize-dropdown-categories-'.$this->id,
					'echo' => 0,
					'show_option_none' => 'All categories',
					'option_none_value' => '',
					'selected' => $this->value(),
				)
			);
 
			// Hackily add in the data link parameter.
			$dropdown = str_replace( '<select', '<select '.$this->get_link(), $dropdown );
 
			printf(
				'<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
				$this->label,
				$dropdown
			);
		}
	}

	// create another class just to change default label
	class WP_Customize_Category_Control_Exclude extends WP_Customize_Control {
		public function render_content() {
			$dropdown = wp_dropdown_categories(
				array(
					'name' => '_customize-dropdown-categories-'.$this->id,
					'echo' => 0,
					'show_option_none' => 'No categories',
					'option_none_value' => '',
					'selected' => $this->value(),
				)
			);
 
			// Hackily add in the data link parameter.
			$dropdown = str_replace( '<select', '<select '.$this->get_link(), $dropdown );
 
			printf(
				'<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
				$this->label,
				$dropdown
			);
		}
	}
}

if (p3_check_file()) {
	return;
}

function pipdig_customize_scripts() {
	wp_enqueue_script( 'pipdig-reset-fonts', get_template_directory_uri().'/inc/customizer-reset-fonts.js', array('jquery') );
}
add_action( 'customize_controls_enqueue_scripts', 'pipdig_customize_scripts' );

function pipdig_fonts_customizer_reset() {
	
	// bail if not in customizer
	if (!is_customize_preview()) {
		return;
	}
	
	delete_transient('pipdig_fonts');
	
	remove_theme_mod('google_font_titles');
	remove_theme_mod('p_header_font');
	remove_theme_mod('header_font_size');
	remove_theme_mod('p_tagline_font');
	remove_theme_mod('p_tagline_size');
	remove_theme_mod('font_body');
	remove_theme_mod('body_font_size');
	remove_theme_mod('p_titles_font');
	remove_theme_mod('post_title_font_size');
	remove_theme_mod('p_widget_title_font');
	remove_theme_mod('pipdig_widget_title_font_size');
	remove_theme_mod('p_navbar_font');
	remove_theme_mod('pipdig_navbar_font_size');
	remove_theme_mod('p_meta_font');
	remove_theme_mod('p_meta_font_size');
	remove_theme_mod('pipdig_you_may_enjoy_size');
	remove_theme_mod('pipdig_related_post_title_size');
	remove_theme_mod('p_content_heading_font');
	remove_theme_mod('p_content_h2_size');
	remove_theme_mod('p_content_h3_size');
	remove_theme_mod('p_content_h4_size');
	
	remove_theme_mod('p_content_h_transform');
	remove_theme_mod('p_content_h_italic');
	remove_theme_mod('p_navbar_font_italic');
	remove_theme_mod('p_navbar_font_transform');
	remove_theme_mod('p_widget_title_font_italic');
	remove_theme_mod('p_widget_title_font_transform');
	remove_theme_mod('p_titles_font_italic');
	remove_theme_mod('p_titles_font_transform');
	remove_theme_mod('p_tagline_font_italic');
	remove_theme_mod('p_tagline_font_transform');
	remove_theme_mod('p_header_font_italic');
	remove_theme_mod('p_header_font_transform');
	remove_theme_mod('p_meta_font_transform');
	remove_theme_mod('p_meta_font_italic');
	
	wp_send_json_success();
}
add_action( 'wp_ajax_pipdig_fonts_customizer_reset', 'pipdig_fonts_customizer_reset' );

function pipdig_customizer_styles() { ?>
	<style>
		#customize-controls span.description {font-size: 90%}
		
		/* font options spacing */
		#customize-control-p_header_font,
		#customize-control-p_tagline_font,
		#customize-control-font_body,
		#customize-control-p_titles_font,
		#customize-control-p_widget_title_font,
		#customize-control-p_widget_title_home_font,
		#customize-control-p_navbar_font,
		#customize-control-p_meta_font,
		#customize-control-pipdig_you_may_enjoy_size,
		#customize-control-p_content_heading_font
		{
			margin-top: 25px;
		}
		
		#sub-accordion-section-pipdig_fonts .customize-control-checkbox {
			display: inline-block !important;
			float: none !important;
			width: 50% !important;
		}
		
	</style>
	<?php
}
add_action( 'customize_controls_print_styles', 'pipdig_customizer_styles', 999 );

// the beef
class pipdig_Customize {
public static function register ( $wp_customize ) {

//remove unwanted sections
//$wp_customize->remove_section( 'title_tagline' );
$wp_customize->remove_section( 'colors' );
$wp_customize->remove_section( 'static_front_page' );
//$wp_customize->remove_section( 'nav' );
$wp_customize->remove_section( 'header_image' );
//$wp_customize->remove_panel( 'widgets' );
$wp_customize->remove_control('custom_logo');

/*
$wp_customize->add_panel( 'pipdig_features', array(
	'priority'		=> 200,
	'capability'	 => 'edit_theme_options',
	'theme_supports' => '',
	'title'		  => 'Extra Features',
	'description'	=> 'Use these options to enable/disable all kinds of extra pipdig features.',
) );
*/

// SECTIONS ================================================================================

$wp_customize->add_section( 'pipdig_header', 
	array(
		'title' => __( 'Header Options', 'pipdig-textdomain' ),
		'priority' => 20,
		'capability' => 'edit_theme_options',
	) 
);

$wp_customize->add_section( 'pipdig_general_options', 
	array(
		'title' => __( 'General Options', 'pipdig-textdomain' ),
		'priority' => 25,
		'capability' => 'edit_theme_options',
	) 
);

$wp_customize->add_section( 'pipdig_layout', 
	array(
		'title' => __( 'Layout Options', 'pipdig-textdomain' ),
		'priority' => 30,
		'capability' => 'edit_theme_options',
	) 
);


$wp_customize->add_section( 'post_layouts', 
	array(
		'title' => __( 'Post Layout Options', 'pipdig-textdomain' ),
		'description' => __("Use these settings to select different post layouts for your blog. You can also change the number of posts displayed by going to 'Settings > Reading' in your dashboard.", 'pipdig-textdomain'),
		'priority' => 31,
		'capability' => 'edit_theme_options',
	) 
);

$wp_customize->add_section( 'pipdig_colors', 
	array(
		'title' => __( 'Color Options', 'pipdig-textdomain' ),
		'priority' => 40,
		'capability' => 'edit_theme_options',
	) 
);

$wp_customize->add_section( 'pipdig_fonts', 
	array(
		'title' => __( 'Font Options', 'pipdig-textdomain' ),
		'description'=> 'You can browse and preview the fonts available on <a href="https://fonts.google.com/" target="_blank" rel="noopener">this page</a>.<br /><br /><input type="submit" class="button-secondary button" value="Reset Fonts" onclick="pipdigResetFonts()">',
		'priority' => 45,
		'capability' => 'edit_theme_options',
	) 
);

$wp_customize->add_section( 'pipdig_backgrounds' , array(
			'title'	  => __( 'Background Image', 'pipdig-textdomain' ),
			'description'=> __( 'Use these options to add a background image to your site. Note, if you would like a solid color please use the options in the "Color Options" section above.', 'pipdig-textdomain' ),
			'priority'	=> 50,
	)
);

$wp_customize->add_section( 'pipdig_posts', 
	array(
		'title' => __( 'Blog Post Options', 'pipdig-textdomain' ),
		'priority' => 55,
		'capability' => 'edit_theme_options',
	) 
);

if ( class_exists('Woocommerce') ) { // woocommerce active?
	$wp_customize->add_section( 'pipdig_woocommerce', 
		array(
			'title' => __( 'Woocommerce Options', 'pipdig-textdomain' ),
			'priority' => 80,
			'capability' => 'edit_theme_options',
		) 
	);
}

// ====================================== gap


// default navigation section goes here.

$wp_customize->add_section( 'pipdig_header_full_width_slider' , array(
			'title'	  => __( 'Full Width Slider', 'pipdig-textdomain' ),
			'description'=> __( 'Use these options to display a full width post slider under the header. NOTE: for a post to be included in the slider it needs to have a "Featured Image" set.', 'pipdig-textdomain' ),
		//'panel' => 'pipdig_features',
			'priority'	=> 53,
	)
);

$wp_customize->add_section( 'pipdig_top_slider' , array(
			'title'	  => __( 'Large Feature Slider', 'pipdig-textdomain' ),
			'description'=> __( 'Use these options to display a large square post slider under the header. NOTE: for a post to be included in the slider it needs to have a "Featured Image" set.', 'pipdig-textdomain' ),
		'capability' => 'edit_theme_options',
		//'panel' => 'pipdig_features',
			'priority'	=> 105,
	)
);

$wp_customize->add_section( 'pipdig_post_carousels' , array(
			'title'	  => __( 'Posts Carousel', 'pipdig-textdomain' ),
			'description'=> __( 'Use these options to display a moving carousel of posts across the bottom of your site.', 'pipdig-textdomain' ),
		'capability' => 'edit_theme_options',
		//'panel' => 'pipdig_features',
			'priority'	=> 115,
	)
);


$wp_customize->add_section( 'pipdig_bloglovin', 
	array(
		'title' => __( "Bloglovin' Widget", 'pipdig-textdomain' ),
		'description' => __( "Use these settings to style our custom Bloglovin' Widget.", 'pipdig-textdomain' ),
		'capability' => 'edit_theme_options',
		//'panel' => 'pipdig_features',
		'priority' => 150,
	) 
);


// Header section ==========================================================================

// Hide the tagline
$wp_customize->add_setting('hide_tagline',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'hide_tagline',
	array(
		'type' => 'checkbox',
		'label' => __( 'Remove the tagline from header', 'pipdig-textdomain' ),
		'section' => 'pipdig_header',
	)
);

// Header image
$wp_customize->add_setting('logo_image',
	array(
		'sanitize_callback' => 'esc_url_raw',
	)
);
$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'upload_header',
			array(
				'label'	  => __( 'Upload a header image', 'pipdig-textdomain' ),
				'section'	=> 'pipdig_header',
				'settings'	=> 'logo_image',
			)
		)
);

// Header image padding top
$wp_customize->add_setting( 'header_image_top_padding', array(
	'default' => 0,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'header_image_top_padding', array(
	'type' => 'range',
	'section' => 'pipdig_header',
	'label' => __( 'Header image spacing (top)', 'pipdig-textdomain' ),
	'input_attrs' => array(
		'min' => 0,
		'max' => 150,
		'step' => 5,
		),
	)
);

// Header image padding bottom
$wp_customize->add_setting( 'header_image_bottom_padding', array(
	'default' => 0,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'header_image_bottom_padding', array(
	'type' => 'range',
	'section' => 'pipdig_header',
	'label' => __( 'Header image spacing (bottom)', 'pipdig-textdomain' ),
	'input_attrs' => array(
		'min' => 0,
		'max' => 150,
		'step' => 5,
		),
	)
);


// Header image width retina
$wp_customize->add_setting( 'header_image_retina_width', array(
	'default' => 0,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'header_image_retina_width', array(
	'type' => 'number',
	'section' => 'pipdig_header',
	'label' => __( 'Header image width (pixels)', 'pipdig-textdomain' ),
	'description' => __( 'This option is not required, but can be useful if you want to force a specific size, or if you want to make the image appear more clear on retina screens. If you are unsure what this means, it is recommended that you do not edit this option.', 'pipdig-textdomain' ),
	'input_attrs' => array(
		'min' => 100,
		//'max' => 150,
		'step' => 1,
		),
	)
);



// Header Full Width Slider ==============================================================================
	
// Activate full width header Slider
$wp_customize->add_setting('header_full_width_slider',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider',
	array(
		'type' => 'checkbox',
		'label' => __('Enable full width slider', 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
	)
);

// show on home or all pages?
$wp_customize->add_setting('header_full_width_slider_home',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_home',
	array(
		'type' => 'checkbox',
		'label' => __('Display on home page only', 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
	)
);

// parallax?
$wp_customize->add_setting('header_full_width_slider_parallax',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_parallax',
	array(
		'type' => 'checkbox',
		'label' => __("Parallax Effect", 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
	)
);

// Number of images to display in slider
$wp_customize->add_setting('header_full_width_slider_posts',
	array(
		'default' => '3',
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('header_full_width_slider_posts',
	array(
		'type' => 'select',
		'label' => __('Number of posts to show in the slider', 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
		'choices' => array(
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
		),
	)
);

// Slide delay in ms
$wp_customize->add_setting('header_full_width_slider_delay',
	array(
		'default' => '4000',
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_delay',
	array(
		'type' => 'select',
		'label' => __('How long should each slide be displayed?', 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
		'choices' => array(
			'3000' => '3 '.__('seconds', 'pipdig-textdomain'),
			'4000' => '4 '.__('seconds', 'pipdig-textdomain'),
			'5000' => '5 '.__('seconds', 'pipdig-textdomain'),
			'6000' => '6 '.__('seconds', 'pipdig-textdomain'),
			'7000' => '7 '.__('seconds', 'pipdig-textdomain'),
			'8000' => '8 '.__('seconds', 'pipdig-textdomain'),
		),
	)
);

// Slide transition
$wp_customize->add_setting('header_full_width_slider_transition',
	array(
		'default' => '5',
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_transition',
	array(
		'type' => 'select',
		'label' => __('Slider animation style', 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
		'choices' => array(
			'5' => __('Fade', 'pipdig-textdomain'),
			'1' => __('Slide', 'pipdig-textdomain'),
		),
	)
);

// Slider height
$wp_customize->add_setting('header_full_width_slider_height',
	array(
		'default' => 420,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_height',
	array(
		'type' => 'number',
		'label' => __('Image height (px)', 'pipdig-textdomain'),
		'section' => 'pipdig_header_full_width_slider',
	)
);

// Choose a category
$wp_customize->add_setting(
	'header_full_width_slider_post_category',
	array(
		'default' => '',
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	new WP_Customize_Category_Control(
		$wp_customize,
		'header_full_width_slider_post_category',
		array(
			'label'	=> __('Only display posts from:', 'pipdig-textdomain'),
			'settings' => 'header_full_width_slider_post_category',
			'section'  => 'pipdig_header_full_width_slider'
		)
	)
);

// Show category in slider
$wp_customize->add_setting('header_full_width_slider_cat',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_cat',
	array(
		'type' => 'checkbox',
		'label' => __( 'Display post category', 'pipdig-textdomain' ),
		'section' => 'pipdig_header_full_width_slider',
	)
);

// Show post excerpt in slider
$wp_customize->add_setting('header_full_width_slider_excerpt',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_excerpt',
	array(
		'type' => 'checkbox',
		'label' => __( 'Include post excerpt', 'pipdig-textdomain' ),
		'section' => 'pipdig_header_full_width_slider',
	)
);

// Show view post button in slider
$wp_customize->add_setting('header_full_width_slider_view_post',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_view_post',
	array(
		'type' => 'checkbox',
		'label' => __( 'Display "View Post" button', 'pipdig-textdomain' ),
		'section' => 'pipdig_header_full_width_slider',
	)
);

$wp_customize->add_setting('header_full_width_slider_metaslider',
	array(
		'sanitize_callback' => 'sanitize_text_field',
	)
);
$wp_customize->add_control(
	'header_full_width_slider_metaslider',
	array(
		'type' => 'text',
		'label' => 'Meta Slider (optional)',
		'description' => 'If you would prefer to display a selection of images (i.e. not just recent posts) please see <a href="https://support.pipdig.co/articles/wordpress-full-width-slider/" target="_blank">this guide</a>',
		'section' => 'pipdig_header_full_width_slider',
	)
);

// Top Slider ==============================================================================
	
// Activate Top Slider
$wp_customize->add_setting('top_slider',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'top_slider',
	array(
		'type' => 'checkbox',
		'label' => __('Enable this slider', 'pipdig-textdomain'),
		'description' => __( 'Displays the Featured Image from your 4 most recent posts in a large slider at the top of the home page', 'pipdig-textdomain' ),
		'section' => 'pipdig_top_slider',
	)
);

// Number of images to display in slider
$wp_customize->add_setting('top_slider_posts',
	array(
		'default' => '3',
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('top_slider_posts',
	array(
		'type' => 'select',
		'label' => __('Number of posts to show in the slider', 'pipdig-textdomain'),
		'section' => 'pipdig_top_slider',
		'choices' => array(
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
		),
	)
);

// Choose a category
$wp_customize->add_setting(
	'top_slider_post_category',
	array(
		'default' => '',
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	new WP_Customize_Category_Control(
		$wp_customize,
		'top_slider_post_category',
		array(
			'label'	=> __('Only display posts from:', 'pipdig-textdomain'),
			'settings' => 'top_slider_post_category',
			'section'  => 'pipdig_top_slider'
		)
	)
);

// Show category in slider
$wp_customize->add_setting('top_slider_cat',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'top_slider_cat',
	array(
		'type' => 'checkbox',
		'label' => __( 'Display post category', 'pipdig-textdomain' ),
		'section' => 'pipdig_top_slider',
	)
);


// Layout section ==========================================================================

// Container width
$wp_customize->add_setting( 'container_width',
	array(
		'default' => 1080,
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'absint',
	)
);

$wp_customize->add_control( 'container_width',
	array(
		'type' => 'number',
		'section' => 'pipdig_layout',
		'label' => __( 'Main Width', 'pipdig-textdomain' ),
		'description' => __( 'The number below is how wide your website is in pixels:', 'pipdig-textdomain' ),
		'input_attrs' => array(
			'min' => 800,
			'max' => 1280,
			'step' => 10,
		),
	)
);

// Disable responsive
$wp_customize->add_setting('disable_responsive',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'disable_responsive',
	array(
		'type' => 'checkbox',
		'label' => __( 'Disable Responsive layout', 'pipdig-textdomain' ),
		'description' => __( 'This theme automatically adapts to different screen sizes. Select this option to disable this, and show the same thing on all devices (not recommended).', 'pipdig-textdomain' ),
		'section' => 'pipdig_layout',
	)
);

// Show sideber on tablets
$wp_customize->add_setting('show_sidebar_tablets',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'show_sidebar_tablets',
	array(
		'type' => 'checkbox',
		'label' => 'Show sidebar on tablets (if sidebar is enabled)',
		'section' => 'pipdig_layout',
	)
);

// Sidebar position
$wp_customize->add_setting( 'sidebar_position',
	array(
		'default' => 'right',
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
	)
);

$wp_customize->add_control( 'sidebar_position',
	array(
		'type' => 'radio',
		'section' => 'pipdig_layout',
		'label' => __( 'Sidebar position:', 'pipdig-textdomain' ),
		'choices' => array(
			'left' => __( 'Left', 'pipdig-textdomain' ),
			'right' => __( 'Right', 'pipdig-textdomain' ),
		),
	)
);

// Full width layout
$wp_customize->add_setting('full_width_layout',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'full_width_layout',
	array(
		'type' => 'checkbox',
		'label' => __( 'Completely disable the sidebar', 'pipdig-textdomain' ),
		'description' => __( 'Removes the sidebar from all pages and posts. This will override any settings below.', 'pipdig-textdomain' ),
		'section' => 'pipdig_layout',
	)
);

// is_home() sidebar
$wp_customize->add_setting('disable_sidebar_home',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'disable_sidebar_home',
	array(
		'type' => 'checkbox',
		'label' => __( 'Disable sidebar on home/blog', 'pipdig-textdomain' ),
		'section' => 'pipdig_layout',
	)
);

// is_archive() sidebar
$wp_customize->add_setting('disable_sidebar_archive',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'disable_sidebar_archive',
	array(
		'type' => 'checkbox',
		'label' => __( 'Disable sidebar on categories/archives', 'pipdig-textdomain' ),
		'section' => 'pipdig_layout',
	)
);

// is_single() sidebar
$wp_customize->add_setting('disable_sidebar_posts',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'disable_sidebar_posts',
	array(
		'type' => 'checkbox',
		'label' => __( 'Disable sidebar on all posts', 'pipdig-textdomain' ),
		'section' => 'pipdig_layout',
	)
);


// is_page() sidebar
$wp_customize->add_setting('disable_sidebar_pages',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'disable_sidebar_pages',
	array(
		'type' => 'checkbox',
		'label' => __( 'Disable sidebar on all pages', 'pipdig-textdomain' ),
		'section' => 'pipdig_layout',
	)
);


if ( class_exists('Woocommerce') ) { // woocommerce active?
	// is_woocommerce() sidebar
	$wp_customize->add_setting('disable_sidebar_woocommerce',
		array(
			'default' => 0,
			'sanitize_callback' => 'absint',
		)
	);
	$wp_customize->add_control(
		'disable_sidebar_woocommerce',
		array(
			'type' => 'checkbox',
			'label' => __( 'Disable sidebar on WooCommerce', 'pipdig-textdomain' ),
			'section' => 'pipdig_layout',
		)
	);
}


// Post Layouts section ==========================================================================

// Homepage layout
$wp_customize->add_setting('home_layout',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('home_layout',
	array(
		'type' => 'radio',
		'label' => __('Homepage layout', 'pipdig-textdomain'),
		'description' => __("This setting will effect posts displayed on the homepage.", 'pipdig-textdomain'),
		'section' => 'post_layouts',
		'choices' => array(
			1 => __('Full blog posts', 'pipdig-textdomain'),
			2 => __('Excerpt/Summary', 'pipdig-textdomain'),
			3 => __('1st post Full, then grid for others', 'pipdig-textdomain'),
			5 => __('1st post Excerpt, then grid for others', 'pipdig-textdomain'),
			4 => __('Grid', 'pipdig-textdomain'),
			//6 => __('Mosaic', 'pipdig-textdomain'),
		),
	)
);

// categories layout
$wp_customize->add_setting('category_layout',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('category_layout',
	array(
		'type' => 'radio',
		'label' => __('Post Categories layout', 'pipdig-textdomain'),
		'description' => __("This setting will effect posts displayed in categories.", 'pipdig-textdomain'),
		'section' => 'post_layouts',
		'choices' => array(
			1 => __('Full blog posts', 'pipdig-textdomain'),
			2 => __('Excerpt/Summary', 'pipdig-textdomain'),
			3 => __('1st post Full, then grid for others', 'pipdig-textdomain'),
			5 => __('1st post Excerpt, then grid for others', 'pipdig-textdomain'),
			4 => __('Grid', 'pipdig-textdomain'),
			//6 => __('Mosaic', 'pipdig-textdomain'),
		),
	)
);

// Excerpt length
$wp_customize->add_setting( 'excerpt_length_num', array(
	'default' => 35,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'excerpt_length_num', array(
	'type' => 'number',
	'label' => __('Excerpt length (words):', 'pipdig-textdomain'),
	'description' => __("This setting will effect all posts when using an excerpt based layout.", 'pipdig-textdomain'),
	'section' => 'post_layouts',
	'input_attrs' => array(
		'min' => 0,
		'max' => 150,
		'step' => 5,
		),
	)
);


// Show excerpt in grid layout
$wp_customize->add_setting('grid_excerpts',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'grid_excerpts',
	array(
		'type' => 'checkbox',
		'label' => __( 'Show post excerpt in grid layout', 'pipdig-textdomain' ),
		'description' => __( 'Select this option to display the post excerpt in any grid based layout', 'pipdig-textdomain' ),
		'section' => 'post_layouts',
	)
);


 
// Post options section ==========================================================================

// Show socialz in post signature?
$wp_customize->add_setting('show_socialz_signature',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'show_socialz_signature',
	array(
		'type' => 'checkbox',
		'label' => __( 'Display Social Media follow section', 'pipdig-textdomain' ),
		'description' => __( 'This option will add links to your social media pages at the end of each post.', 'pipdig-textdomain' ),
		'section' => 'pipdig_posts',
	)
);

// Signature image
$wp_customize->add_setting('post_signature_image',
	array(
		'sanitize_callback' => 'esc_url_raw',
	)
);
$wp_customize->add_control(
	new WP_Customize_Image_Control(
		$wp_customize,
		'signature_image',
		array(
			'label' => __( 'Post signature image', 'pipdig-textdomain' ),
			'description' => __( 'This image will be shown in the footer of your posts', 'pipdig-textdomain' ),
			'section' => 'pipdig_posts',
			'settings' => 'post_signature_image',
		)
	)
);

// Show author under title?
$wp_customize->add_setting('show_author',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'show_author',
	array(
		'type' => 'checkbox',
		'label' => __( 'Show author name on posts', 'pipdig-textdomain' ),
		'description' => __( "Shows the author's name next to the date on each post header", 'pipdig-textdomain' ),
		'section' => 'pipdig_posts',
	)
);

// Show cat under title?
$wp_customize->add_setting('show_main_cat',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'show_main_cat',
	array(
		'type' => 'checkbox',
		'label' => __( 'Display main post category', 'pipdig-textdomain' ),
		'description' => __( "Display the post category on each post header", 'pipdig-textdomain' ),
		'section' => 'pipdig_posts',
	)
);

// Hide dates
$wp_customize->add_setting('hide_dates',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'hide_dates',
	array(
		'type' => 'checkbox',
		'label' => __( 'Remove dates from posts', 'pipdig-textdomain' ),
		'description' => __( 'This will hide the date from showing on any posts everywhere on your site.', 'pipdig-textdomain' ),
		'section' => 'pipdig_posts',
	)
);

// Hide comments link
$wp_customize->add_setting('hide_comments_link',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('hide_comments_link',
	array(
		'type' => 'checkbox',
		'label' => __( 'Remove link to comments', 'pipdig-textdomain' ),
		'description' => __( 'This option will remove the "No comments" or "x Comments" link from posts on the home page', 'pipdig-textdomain' ),
		'section' => 'pipdig_posts',
	)
);

// Hide tags
$wp_customize->add_setting('hide_post_tags',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('hide_post_tags',
	array(
		'type' => 'checkbox',
		'label' => __( 'Remove tags from posts', 'pipdig-textdomain' ),
		'description' => __( 'Select this option if you would prefer for your tags not to be shown at the bottom of posts', 'pipdig-textdomain' ),
		'section' => 'pipdig_posts',
	)
);




// Post carousels section ==========================================================================

// Enable post carousel Footer
$wp_customize->add_setting('posts_carousel_footer',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'posts_carousel_footer',
	array(
		'type' => 'checkbox',
		'label' => __('Enable post carousel in footer', 'pipdig-textdomain'),
		'description' => __('This will display 15 random posts in the footer of your site. Select a date range from below for posts to include.', 'pipdig-textdomain'),
		'section' => 'pipdig_post_carousels',
	)
);

// Date criteria for posts in carousel
$wp_customize->add_setting('posts_carousel_dates',
	array(
		'default' => '1 year ago',
		'sanitize_callback' => 'sanitize_text_field',
	)
);
$wp_customize->add_control('posts_carousel_dates',
	array(
		'type' => 'select',
		'label' => __('Date range for posts:', 'pipdig-textdomain'),
		'section' => 'pipdig_post_carousels',
		'choices' => array(
			'1 year ago' => __('1 Year', 'pipdig-textdomain'),
			'1 month ago' => __('1 Month', 'pipdig-textdomain'),
			'1 week ago' => __('1 Week', 'pipdig-textdomain'),
			'' => __('All Time', 'pipdig-textdomain'),
		),
	)
);




// General Options section ==========================================================================

// Add search toggle to navbar (scotch)
$wp_customize->add_setting('site_top_search',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'site_top_search',
	array(
		'type' => 'checkbox',
		'label' => __( 'Search button', 'pipdig-textdomain' ),
		'section' => 'p3_navbar_icons_section',
		'priority' => 2,
	)
);


// Show socialz in footer with counter?
$wp_customize->add_setting('social_count_footer',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'social_count_footer',
	array(
		'type' => 'checkbox',
		'label' => __( 'Display social counter in the Footer', 'pipdig-textdomain' ),
		'description' => __( 'Select this option to show your social media follower counts at the bottom of your website. Yuo will need to add your links to "pipdig > Social Links" first.', 'pipdig-textdomain' ),
		'section' => 'pipdig_general_options',
	)
);

// Left align menu
$wp_customize->add_setting('left_align_menu',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'left_align_menu',
	array(
		'type' => 'checkbox',
		'label' => __( 'Left-align the navbar', 'pipdig-textdomain' ),
		'description' => __( 'Select this option to align the navbar menu items to the left, rather than in the center.', 'pipdig-textdomain' ),
		'section' => 'pipdig_general_options',
	)
);

$wp_customize->add_setting('pipdig_lazy',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control(
	'pipdig_lazy',
	array(
		'type' => 'checkbox',
		'label' => '"Lazy Load" images to speed up your homepage.',
		'section' => 'pipdig_general_options',
	)
);

$wp_customize->add_setting('copyright_statement',
	array(
		'sanitize_callback' => 'pipdig_sanitize_text_html',
	)
);
$wp_customize->add_control(
	'copyright_statement',
	array(
		'type' => 'text',
		'label' => __( 'Copyright text in footer:', 'pipdig-textdomain' ),
		'section' => 'pipdig_general_options',
		'input_attrs' => array(
			'placeholder' => '&copy; '.date('Y').' '.get_bloginfo('name'),
		),
	)
);





// Background Image section ===============================================
	
$wp_customize->add_setting('pipdig_background_image',
	array(
		'sanitize_callback' => 'esc_url_raw',
	)
);
$wp_customize->add_control(
	new WP_Customize_Image_Control(
		$wp_customize,
		'upload_background_image',
		array(
			'label'	  => __( 'Upload image', 'pipdig-textdomain' ),
			'section'	=> 'pipdig_backgrounds',
			'settings'	=> 'pipdig_background_image',
		)
	)
);

// Background image repeat or backstretch
$wp_customize->add_setting('pipdig_background_repeats',
	array(
		'default' => 'repeat',
		'sanitize_callback' => 'sanitize_text_field',
	)
);
$wp_customize->add_control('pipdig_background_repeats',
	array(
		'type' => 'select',
		'label' => __('Background image layout:', 'pipdig-textdomain'),
		'section' => 'pipdig_backgrounds',
		'choices' => array(
			'repeat' => __('Tile', 'pipdig-textdomain'),
			'repeat-x' => __('Tile Horizontally', 'pipdig-textdomain'),
			'repeat-y' => __('Tile Vertically', 'pipdig-textdomain'),
			'' => __('Stretch Full Screen', 'pipdig-textdomain'),
		),
	)
);





			
// Woocommerce section ==========================================================================	
if ( class_exists('Woocommerce') ) { // woocommerce active?

// number of products to display
$wp_customize->add_setting( 'number_of_products_index', array(
	'default' => 20,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'number_of_products_index', array(
	'type' => 'number',
	'section' => 'pipdig_woocommerce',
	'label' => __( 'Number of Products displayed in the shop:', 'pipdig-textdomain' ),
	'input_attrs' => array(
		'min' => 4,
		'max' => 40,
		'step' => 4,
		),
	)
);

// Show sorting options
$wp_customize->add_setting('pipdig_woo_sorting',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('pipdig_woo_sorting',
	array(
		'type' => 'checkbox',
		'label' => 'Display sorting options on product categories/shop',
		'section' => 'pipdig_woocommerce',
	)
);

// Hide social sharing icons
$wp_customize->add_setting('hide_social_sharing_products',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('hide_social_sharing_products',
	array(
		'type' => 'checkbox',
		'label' => __( 'Remove sharing icons from products', 'pipdig-textdomain' ),
		'section' => 'pipdig_woocommerce',
	)
);

} // end if check woo



	
	// background color
	$wp_customize->add_setting('pipdig_bloglovin_widget_background_color',
		array(
			'default' => '#ffffff',
			//'transport'=>'postMessage',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_bloglovin_widget_background_color',
		array(
			'label' => __( 'Background color', 'pipdig-textdomain' ),
			'section' => 'pipdig_bloglovin',
			'settings' => 'pipdig_bloglovin_widget_background_color',
		)
		)
	);

	// border color
	$wp_customize->add_setting('pipdig_bloglovin_widget_border_color',
		array(
			'default' => '#cccccc',
			//'transport'=>'postMessage',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_bloglovin_widget_border_color',
		array(
			'label' => __( 'Border color', 'pipdig-textdomain' ),
			'section' => 'pipdig_bloglovin',
			'settings' => 'pipdig_bloglovin_widget_border_color',
		)
		)
	);

	// text color
	$wp_customize->add_setting('pipdig_bloglovin_widget_text_color',
		array(
			'default' => '#000000',
			//'transport'=>'postMessage',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_bloglovin_widget_text_color',
		array(
			'label' => __( 'Text color', 'pipdig-textdomain' ),
			'section' => 'pipdig_bloglovin',
			'settings' => 'pipdig_bloglovin_widget_text_color',
		)
		)
	);

	$wp_customize->add_setting('pipdig_bloglovin_widget_icon',
		array(
			'default' => 'heart',
			//'sanitize_callback' => 'bloglovin_widget_sanitize_icon',
		)
	);
	 
	$wp_customize->add_control('pipdig_bloglovin_widget_icon',
		array(
			'type' => 'radio',
			'label' => __( 'Widget Icon', 'pipdig-textdomain' ),
			'section' => 'pipdig_bloglovin',
			'choices' => array(
				'heart' => __( 'Heart', 'pipdig-textdomain' ),
				'plus' => __( 'Plus', 'pipdig-textdomain' ),
				'none' => __( 'None', 'pipdig-textdomain' ),
			),
		)
	);
	


// Use Bloglovin official widget?
$wp_customize->add_setting('pipdig_bloglovin_widget_official',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('pipdig_bloglovin_widget_official',
	array(
		'type' => 'checkbox',
		'label' => __( "Use the official Bloglovin' widget", 'pipdig-textdomain' ),
		'description' => __( "Select this option if you would prefer to use the official Bloglovin' widget.", 'pipdig-textdomain' ),
		'section' => 'pipdig_bloglovin',
	)
);


// Theme colors section ==========================================================================

// Navbar background color
$wp_customize->add_setting('navbar_background_color',
	array(
		'default' => '#ffffff',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'navbar_background_color',
	array(
		'label' => __( 'Navbar background color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'navbar_background_color',
	)
	)
);

// Navbar text color
$wp_customize->add_setting('navbar_text_color',
	array(
		'default' => '#000000',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'navbar_text_color',
	array(
		'label' => __( 'Navbar text color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'navbar_text_color',
	)
	)
);

// Navbar text hover color
$wp_customize->add_setting('navbar_text_color_hover',
	array(
		'default' => '#999999',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'navbar_text_color_hover',
	array(
		'label' => __( 'Navbar text hover color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'navbar_text_color_hover',
	)
	)
);

// Header text color
$wp_customize->add_setting('header_color',
	array(
		'default' => '#000000',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'header_color',
	array(
		'label' => __( 'Header text color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'header_color',
	)
	)
);

// Tagline text color
$wp_customize->add_setting('tagline_color',
	array(
		'default' => '#666666',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'tagline_color',
	array(
		'label' => __( 'Tagline text color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'tagline_color',
	)
	)
);


// Outer background color
$wp_customize->add_setting('outer_background_color',
	array(
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'outer_background_color',
	array(
		'label' => __( 'Outer background color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'outer_background_color',
	)
	)
);

// Content background color
$wp_customize->add_setting('content_background_color',
	array(
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'content_background_color',
	array(
		'label' => __( 'Content background color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'content_background_color',
	)
	)
);

// Main text color
$wp_customize->add_setting('p_main_text_color',
	array(
		'default' => '#333333',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'p_main_text_color',
	array(
		'label' => __( 'Main text color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'p_main_text_color',
	)
	)
);

// Post title color
$wp_customize->add_setting('post_title_color',
	array(
		'default' => '#000000',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'post_title_color',
	array(
		'label' => __( 'Post title color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'post_title_color',
	)
	)
);


// Post title hover color
$wp_customize->add_setting('post_title_hover_color',
	array(
		'default' => '#999999',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'post_title_hover_color',
	array(
		'label' => __( 'Post title hover color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'post_title_hover_color',
	)
	)
);

// Post links color
$wp_customize->add_setting('post_links_color',
	array(
		'default' => '#999999',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'post_links_color',
	array(
		'label' => __( 'Post links color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'post_links_color',
	)
	)
);


// Post links hover color
$wp_customize->add_setting('post_links_hover_color',
	array(
		'default' => '#000000',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'post_links_hover_color',
	array(
		'label' => __( 'Post links hover color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'post_links_hover_color',
	)
	)
);

// main border color
$wp_customize->add_setting('main_border_color', // needs a prefix for some reason...
	array(
		'default' => '#cccccc',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'main_border_color',
	array(
		'label' => __( 'Main border color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'main_border_color',
	)
	)
);

// Widget title background color
$wp_customize->add_setting('pipdig_widget_title_background_color', // needs a prefix for some reason...
	array(
		'default' => '#ffffff',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_widget_title_background_color',
	array(
		'label' => __( 'Widget title background color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'pipdig_widget_title_background_color',
	)
	)
);

// Widget title text color
$wp_customize->add_setting('pipdig_widget_title_text_color',
	array(
		'default' => '#000000',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_widget_title_text_color',
	array(
		'label' => __( 'Widget title text color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'pipdig_widget_title_text_color',
	)
	)
);


// read more background color
$wp_customize->add_setting('pipdig_readmore_background_color', // needs a prefix for some reason...
	array(
		'default' => '#ffffff',
		//'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_readmore_background_color',
	array(
		'label' => __( '"View Post" button background', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'pipdig_readmore_background_color',
	)
	)
);

// read more text color
$wp_customize->add_setting('pipdig_readmore_text_color',
	array(
		'default' => '#000000',
		//'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pipdig_readmore_text_color',
	array(
		'label' => __( '"View Post" button text', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'pipdig_readmore_text_color',
	)
	)
);


// Social Icon color
$wp_customize->add_setting('social_color',
	array(
		'default' => '#111111',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'social_color',
	array(
		'label' => __( 'Social Icon color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'social_color',
	)
	)
);

// Social Icon Hover color
$wp_customize->add_setting('social_hover_color',
	array(
		'default' => '#999999',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'social_hover_color',
	array(
		'label' => __( 'Social Icon Hover color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'social_hover_color',
	)
	)
);

// Footer bar background color
$wp_customize->add_setting('footer_bar_bg_color',
	array(
		'default' => '#111111',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_bg_color',
	array(
		'label' => __( 'Footer Bar Background Color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'footer_bar_bg_color',
	)
	)
);

// Footer bar background color
$wp_customize->add_setting('footer_bar_text_color',
	array(
		'default' => '#ffffff',
		'transport'=>'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	)
);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_text_color',
	array(
		'label' => __( 'Footer Bar Text Color', 'pipdig-textdomain' ),
		'section' => 'pipdig_colors',
		'settings' => 'footer_bar_text_color',
	)
	)
);



// Font sizes =====================================================================

$fonts_array = pipdig_get_fonts();

// Feature titles font
$wp_customize->add_setting('google_font_titles',
	array(
		'default' => 'Oswald',
		'sanitize_callback' => 'sanitize_text_field',
	)
);
$wp_customize->add_control('google_font_titles',
	array(
		'type' => 'select',
		'label' => __('Main feature/titles font', 'pipdig-textdomain'),
		'section' => 'pipdig_fonts',
		'choices' => $fonts_array,
	)
);

if (!function_exists('p3_build_cc')) {
	function p3_build_cc() {
		// de nada
	}
}

// header
$slugs = array('p_header_font', 'header_font_size', 'p_header_font_transform', 'p_header_font_italic');
p3_build_cc($wp_customize, $fonts_array, $slugs, 'Site header/logo', 'Oswald', 68, 1, 0, false, 'https://i.imgur.com/Sx5CfMS.png');

// tagline
$slugs = array('p_tagline_font', 'p_tagline_size', 'p_tagline_font_transform', 'p_tagline_font_italic');
p3_build_cc($wp_customize, $fonts_array, $slugs, 'Site tagline', 'Oswald', 15, 1, 0, false, 'https://i.imgur.com/dKCPeYv.png');

// body
$slugs = array('font_body', 'body_font_size');
p3_build_cc($wp_customize, $fonts_array, $slugs, 'Main body', 'Verdana', 12, false, false, false, 'https://i.imgur.com/08i7o6K.jpg');

// post titles
$slugs = array('p_titles_font', 'post_title_font_size', 'p_titles_font_transform', 'p_titles_font_italic');
p3_build_cc($wp_customize, $fonts_array, $slugs, 'Post titles', 'Oswald', 20, 1, 0, false, 'https://i.imgur.com/T6eeoZp.png');

// widget titles
$slugs = array('p_widget_title_font', 'pipdig_widget_title_font_size', 'p_widget_title_font_transform', 'p_widget_title_font_italic');
p3_build_cc($wp_customize, $fonts_array, $slugs, 'Widget titles', 'Oswald', 12, 1, 0, false, 'https://i.imgur.com/HuQ95Bm.jpg');

// navbar
$slugs = array('p_navbar_font', 'pipdig_navbar_font_size', 'p_navbar_font_transform', 'p_navbar_font_italic');
p3_build_cc($wp_customize, $fonts_array, $slugs, 'Main menu/navbar', 'Oswald', 12, 1, 0, false, 'https://i.imgur.com/vwhSUqn.png');



// 'You may also enjoy' font size
$wp_customize->add_setting( 'pipdig_you_may_enjoy_size', array(
	'default' => 16,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'pipdig_you_may_enjoy_size', array(
	'type' => 'number',
	'section' => 'pipdig_fonts',
	'label' => '"You may also enjoy:" size',
	'input_attrs' => array(
		'min' => 4,
		'max' => 40,
		'step' => 1,
		),
	)
);

// 'You may also enjoy' post title font size
$wp_customize->add_setting( 'pipdig_related_post_title_size', array(
	'default' => 13,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'pipdig_related_post_title_size', array(
	'type' => 'number',
	'section' => 'pipdig_fonts',
	'label' => '"You may also enjoy" post titles size',
	'input_attrs' => array(
		'min' => 4,
		'max' => 40,
		'step' => 1,
		),
	)
);

// post content headings font
$wp_customize->add_setting('p_content_heading_font',
	array(
		'default' => 'Oswald',
		'sanitize_callback' => 'sanitize_text_field',
	)
);
$wp_customize->add_control('p_content_heading_font',
	array(
		'type' => 'select',
		'label' => 'Post Content Headings Font',
		'description' => '<a href="https://i.imgur.com/NKT5JmZ.png" target="_blank" rel="noopener">'.__("What's this?", "pipdig-textdomain").'</a>',
		'section' => 'pipdig_fonts',
		'choices' => $fonts_array,
	)
);

// .entry-content h2 size
$wp_customize->add_setting( 'p_content_h2_size', array(
	'default' => 17,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'p_content_h2_size', array(
	'type' => 'number',
	'section' => 'pipdig_fonts',
	'label' => 'Content Heading 2 (H2) Size',
	'input_attrs' => array(
		'min' => 4,
		'max' => 40,
		'step' => 1,
		),
	)
);
// .entry-content h3 size
$wp_customize->add_setting( 'p_content_h3_size', array(
	'default' => 16,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'p_content_h3_size', array(
	'type' => 'number',
	'section' => 'pipdig_fonts',
	'label' => 'Content Heading 3 (H3) Size',
	'input_attrs' => array(
		'min' => 4,
		'max' => 40,
		'step' => 1,
		),
	)
);
// .entry-content h4 size
$wp_customize->add_setting( 'p_content_h4_size', array(
	'default' => 12,
	'capability' => 'edit_theme_options',
	'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control( 'p_content_h4_size', array(
	'type' => 'number',
	'section' => 'pipdig_fonts',
	'label' => 'Content Heading 4 (H4) Size',
	'input_attrs' => array(
		'min' => 4,
		'max' => 40,
		'step' => 1,
		),
	)
);

$wp_customize->add_setting('p_content_h_transform',
	array(
		'default' => 1,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('p_content_h_transform',
	array(
		'type' => 'checkbox',
		'label' => 'Uppercase',
		'section' => 'pipdig_fonts',
	)
);

$wp_customize->add_setting('p_content_h_italic',
	array(
		'default' => 0,
		'sanitize_callback' => 'absint',
	)
);
$wp_customize->add_control('p_content_h_italic',
	array(
		'type' => 'checkbox',
		'label' => 'Italic',
		'section' => 'pipdig_fonts',
	)
);



}




public static function live_preview() {
	wp_enqueue_script( 
		'pipdig-themecustomizer', // Give the script a unique ID
		get_template_directory_uri().'/inc/customizer.js', // Define the path to the JS file
		array(  'jquery', 'customize-preview' ), // Define dependencies
		'', // Define a version (optional) 
		true // Specify whether to put in footer (leave this true)
	);
	}
}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'pipdig_Customize' , 'register' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'pipdig_Customize' , 'live_preview' ) );

function pipdig_sanitize_text_html( $input ) {
	return wp_kses_post( force_balance_tags( $input ) );
}



function pipdig_customizer_head_styles() {

	$styles_output = '';
	
	// Main text color
	$p_main_text_color = get_theme_mod( 'p_main_text_color', '#333333' ); 
	if ($p_main_text_color != '#333333') {
		$styles_output .= 'body, .entry-meta, .entry-footer a, .entry-meta a:hover, .entry-meta a:focus, .addthis_toolbox .fa, .page-numbers a, .page-numbers span {color: '.$p_main_text_color.'}';
	}
	
	// Outer background color
	$outer_background_color = get_theme_mod( 'outer_background_color', '#ffffff' ); 
	if ($outer_background_color != '#ffffff') {
		$styles_output .= 'body{background:'.$outer_background_color.'}';
	}
	
	// Content background color
	$content_background_color = get_theme_mod( 'content_background_color', '#ffffff' ); 
	if ($content_background_color != '#ffffff') {
		$styles_output .= '.container,.widget-title span,.date-bar-white-bg,#trendingz h2 span{background:'.$content_background_color.'}';
	}
	
	// Navbar background color
	$navbar_background_color = get_theme_mod( 'navbar_background_color', '#ffffff' ); 
	if ($navbar_background_color != '#ffffff') {
		$styles_output .= '.site-top,.menu-bar ul ul,.slicknav_menu {border:0;background:'.$navbar_background_color.'}';
	}
	
	// Navbar text color
	$navbar_text_color = get_theme_mod( 'navbar_text_color', '#000000' ); 
	if ($navbar_text_color != '#000000') {
		$styles_output .= '.menu-bar ul li a,.slicknav_brand,.slicknav_brand a,.slicknav_nav a,.slicknav_menu .slicknav_menutxt, .pipdig_navbar_search input {color:'.$navbar_text_color.'}';
		$styles_output .= '.pipdig_navbar_search input::-webkit-input-placeholder {color:'.$navbar_text_color.'}';
		$styles_output .= '.pipdig_navbar_search input:-moz-placeholder {color:'.$navbar_text_color.'}';
		$styles_output .= '.pipdig_navbar_search input::-moz-placeholder {color:'.$navbar_text_color.'}';
		$styles_output .= '.pipdig_navbar_search input:-ms-input-placeholder {color:'.$navbar_text_color.'}';
	}
	
	// Navbar text color hover
	$navbar_text_color_hover = get_theme_mod( 'navbar_text_color_hover', '#999999' ); 
	if ($navbar_text_color_hover != '#999999') {
		$styles_output .= '.menu-bar ul li a:hover,.menu-bar ul ul li > a:hover,.menu-bar ul ul li:hover > a{color:'.$navbar_text_color_hover.'}';
	}

	// Post title color
	$post_title_color = get_theme_mod( 'post_title_color', '#000000' ); 
	if ($post_title_color != '#000000') {
		$styles_output .= '.entry-title,.entry-title a,.entry-title a:active,.entry-title a:visited, #p3_featured_cats h4, .p3_featured_post_widget_post_title {color:'.$post_title_color.'}';
	}

	// Post title hover color
	$post_title_hover_color = get_theme_mod( 'post_title_hover_color', '#999999' ); 
	if ($post_title_hover_color != '#999999') {
		$styles_output .= '.entry-title a:hover{color:'.$post_title_hover_color.'}';
	}

	// Post links color
	$post_links_color = get_theme_mod( 'post_links_color', '#999999' ); 
	if ($post_links_color != '#999999') {
		$styles_output .= '.site-sidebar a, .entry-content a:not(.more-link){color:'.$post_links_color.'}';
	}

	// Post links hover color
	$post_links_hover_color = get_theme_mod( 'post_links_hover_color', '#000000' ); 
	if ($post_links_hover_color != '#000000') {
		$styles_output .= '.site-sidebar a:hover, .entry-content a:hover{color:'.$post_links_hover_color.'}';
	}
	
	// Main border color
	$main_border_color = get_theme_mod( 'main_border_color', '#cccccc' ); 
	if ($main_border_color != '#cccccc') {
		$styles_output .= 'hr, table > thead > tr > th, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > td, table > tbody > tr > td, table > tfoot > tr > td .widget-title, .slicknav_menu, .site-top, .comment-list, .comment, .pingback, .entry-header .entry-meta, .more-link, .woocommerce table.shop_table, .woocommerce table.shop_table td, .woocommerce-cart table.cart td.actions .coupon .input-text, .woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce .woocommerce-message, .woocommerce div.product .woocommerce-tabs ul.tabs::before, .woocommerce table.shop_table tbody th, .woocommerce table.shop_table tfoot td, .woocommerce table.shop_table tfoot th, .woocommerce #reviews #comments ol.commentlist li .comment-text, ul.page-numbers, .widget-title {border-color:'.$main_border_color.'}';
	}

	// Widget title background color
	$pipdig_widget_title_background_color = get_theme_mod( 'pipdig_widget_title_background_color', '#ffffff' ); 
	if ($pipdig_widget_title_background_color != '#ffffff') {
		$styles_output .= '.widget-title,.top-slider-section .read-more{border:0;background:'.$pipdig_widget_title_background_color.'}.widget-title{margin-bottom:15px;}';
	}
	
	// Widget title text color
	$pipdig_widget_title_text_color = get_theme_mod( 'pipdig_widget_title_text_color', '#000000' ); 
	if ($pipdig_widget_title_text_color != '#000000') {
		$styles_output .= '.widget-title,.top-slider-section .read-more{color:'.$pipdig_widget_title_text_color.'}';
	}

	// Header text color
	$header_color = get_theme_mod( 'header_color', '#000000' );
	if ($header_color != '#000000') {
		$styles_output .= '.site-title a{color:'.$header_color.'}';
	}
	
	// tagline color
	$tagline_color = get_theme_mod( 'tagline_color', '#666666' );
	if ($tagline_color != '#666666') {
		$styles_output .= '.site-description{color:'.$tagline_color.'}';
	}

	// Social Icon color
	$social_color = get_theme_mod( 'social_color', '#111111' );
	if ($social_color != '#111111') {
		$styles_output .= '.socialz a{color:'.$social_color.'}';
	}

	// Social Icon Hover color
	$social_hover_color = get_theme_mod( 'social_hover_color', '#999999' );
	if ($social_hover_color != '#999999') {
		$styles_output .= '.socialz a:hover, #p3_social_sidebar a:hover{color:'.$social_hover_color.'}';
	}
	
	// read more background color
	$pipdig_readmore_background_color = get_theme_mod( 'pipdig_readmore_background_color', '#ffffff' ); 
	if ($pipdig_readmore_background_color != '#ffffff') {
		$styles_output .= '.more-link{background:'.$pipdig_readmore_background_color.';border:0}';
	}
	
	// read more text color
	$pipdig_readmore_text_color = get_theme_mod( 'pipdig_readmore_text_color', '#000000' ); 
	if ($pipdig_readmore_text_color != '#000000') {
		$styles_output .= '.more-link{color:'.$pipdig_readmore_text_color.'}';
	}
	
	// Footer bar background color
	$footer_bar_bg_color = get_theme_mod( 'footer_bar_bg_color', '#111111' ); 
	if ($footer_bar_bg_color != '#111111') {
		$styles_output .= '.site-footer,.social-footer-outer,.p3_instagram_footer_title_bar{background:'.$footer_bar_bg_color.'}';
	}
	
	// Footer bar text color
	$footer_bar_text_color = get_theme_mod( 'footer_bar_text_color', '#ffffff' ); 
	if ($footer_bar_text_color != '#ffffff') {
		$styles_output .= '.site-footer,.site-footer a,.site-footer a:hover,.social-footer,.social-footer a,.p3_instagram_footer_title_bar a, .p3_instagram_footer_title_bar a:hover, .p3_instagram_footer_title_bar a:focus, .p3_instagram_footer_title_bar a:visited{color:'.$footer_bar_text_color.'}';
	}


	// FONTS ============
	
	// Body font size
	$body_font_size = get_theme_mod( 'body_font_size', 12 );
	if ($body_font_size != 12) {
		$styles_output .= 'body{font-size:'.intval($body_font_size).'px}';
	}
	
	// Header font size
	$header_font_size = get_theme_mod( 'header_font_size', 68 );
	if ($header_font_size != 68) {
		$styles_output .= '.site-title{font-size:'.intval($header_font_size).'px}@media only screen and (max-width:769px){.site-title {font-size:40px;font-size:9vw}}';
	}
	
	// Post title font size
	$post_title_font_size = get_theme_mod( 'post_title_font_size', 20 );
	if ($post_title_font_size != 20) {
		$styles_output .= '.entry-title, .page-title, .p_post_titles_font, .slide-h2 {font-size:'.intval($post_title_font_size).'px}.grid-title{height:'.intval($post_title_font_size).'px;line-height:'.intval($post_title_font_size).'px}@media only screen and (max-width:719px){.grid-title{height:auto}}';
	}

	// Widget title font size
	$pipdig_widget_title_font_size = get_theme_mod( 'pipdig_widget_title_font_size', 12 );
	if ($pipdig_widget_title_font_size != 12) {
		$styles_output .= '.widget-title{font-size:'.intval($pipdig_widget_title_font_size).'px}';
	}
	
	// menu font size
	$pipdig_navbar_font_size = get_theme_mod( 'pipdig_navbar_font_size', 12 );
	if ($pipdig_navbar_font_size != 12) {
		$styles_output .= '.menu-bar ul li a, .slicknav_menu{font-size:'.intval($pipdig_navbar_font_size).'px}';
	}
	
	// "you may enjoy" title font size
	$pipdig_you_may_enjoy_size = get_theme_mod( 'pipdig_you_may_enjoy_size', 16 );
	if ($pipdig_you_may_enjoy_size != 16) {
		$styles_output .= '.entry-content .pipdig_p3_related_posts h3, .pipdig_p3_related_posts h3{font-size:'.intval($pipdig_you_may_enjoy_size).'px}';
	}
	
	// "you may enjoy" post title font size
	$pipdig_related_post_title_size = get_theme_mod( 'pipdig_related_post_title_size', 13 );
	if ($pipdig_related_post_title_size != 13) {
		$styles_output .= '.pipdig_p3_related_title a{font-size:'.intval($pipdig_related_post_title_size).'px}';
	}

	// Google font titles
	$google_font_titles = get_theme_mod( 'google_font_titles', 'Oswald' );
	if ($google_font_titles != 'Oswald') {
		$google_font_titles = str_replace('+', ' ', $google_font_titles);
		$styles_output .= '.btn,button,input[type="button"],input[type="reset"],input[type="submit"],.menu-bar ul li a,.slicknav_menu,.site-title,.site-description,.entry-title,.entry-meta,.page-title,.page-links a,.site-main [class*="navigation"] a,.site-main .post-navigation a,.site-main .post-navigation .meta-nav,.comment-meta,.comment-author,.widget-title,.comment-date,.menu-titlez,.sharez,.wpp-post-title,.more-link,.ls-slide .read-more,.woocommerce span.onsale,.woocommerce table.shop_table th,.woocommerce a.button,.woocommerce div.product .woocommerce-tabs ul.tabs li,.pipdig-slider-cats,.top-slider-section .read-more,#imagelightbox-caption,.page-numbers a,.page-numbers span,.social-footer,h1,h2,h3,h4,h5,h6,.cat-item a,.widget_archive a,#mosaic-nav,.slide-h2,.read-more{font-family: "'.$google_font_titles.'"}';
	}
		
	// header font
	$p_header_font = get_theme_mod( 'p_header_font', 'Oswald' );
	if ($p_header_font != 'Oswald') {
		$p_header_font = str_replace('+', ' ', $p_header_font);
		$styles_output .= '.site-title {letter-spacing: 0; font-family: "'.$p_header_font.'"}';
	}
	
	if (get_theme_mod('p_header_font_transform', 1) == false) {
		$styles_output .= '.site-title {text-transform: none}';
	}
	if (get_theme_mod('p_header_font_italic')) {
		$styles_output .= '.site-title {font-style: italic}';
	}
	
	
	//Tagline font
	$p_tagline_font = get_theme_mod( 'p_tagline_font', 'Oswald' );
	if ($p_tagline_font != 'Oswald') {
		$p_tagline_font = str_replace('+', ' ', $p_tagline_font);
		$styles_output .= '.site-description {letter-spacing: 0; font-family: "'.$p_tagline_font.'"}';
	}
	
	if (get_theme_mod('p_tagline_font_transform', 1) == false) {
		$styles_output .= '.site-description {text-transform: none}';
	}
	if (get_theme_mod('p_tagline_font_italic')) {
		$styles_output .= '.site-description {font-style: italic}';
	}
	
	// Tagline font size
	$p_tagline_size = get_theme_mod( 'p_tagline_size', 15 );
	if ($p_tagline_size != 15) {
		$styles_output .= '.site-description {font-size:'.$p_tagline_size.'px}';
	}
	
	// Main body font
	$font_body = get_theme_mod( 'font_body', 'Verdana' );
	if ($font_body != 'Verdana') {
		$font_body = str_replace('+', ' ', $font_body);
		$styles_output .= 'body {font-family: "'.$font_body.'"}';
	}
		
	// widget titles font
	$p_widget_title_font = get_theme_mod( 'p_widget_title_font', 'Oswald' );
	if ($p_widget_title_font != 'Oswald') {
		$p_widget_title_font = str_replace('+', ' ', $p_widget_title_font);
		$styles_output .= '.widget-title {letter-spacing: 0; font-family: "'.$p_widget_title_font.'"}';
	}
	
	if (get_theme_mod('p_widget_title_font_transform', 1) == false) {
		$styles_output .= '.widget-title {text-transform: none}';
	}
	if (get_theme_mod('p_widget_title_font_italic')) {
		$styles_output .= '.widget-title {font-style: italic}';
	}
	
	
	// post title font
	$p_titles_font = get_theme_mod( 'p_titles_font', 'Oswald' );
	if ($p_titles_font != 'Oswald') {
		$p_titles_font = str_replace('+', ' ', $p_titles_font);
		$styles_output .= '.p_post_titles_font, .slide-h2, .entry-title,.page-title,.pipdig_p3_related_title a, .entry-content .pipdig_p3_related_posts h3, .pipdig_p3_related_posts h3, .p3_popular_posts_widget h4, .comment-reply-title, .top-slider-section .post-header h2 {letter-spacing: 0; font-family: "'.$p_titles_font.'"}';
	}
	
	if (get_theme_mod('p_titles_font_transform', 1) == false) {
		$styles_output .= '.entry-title,.page-title,.pipdig_p3_related_title a, .entry-content .pipdig_p3_related_posts h3, .pipdig_p3_related_posts h3, .p3_popular_posts_widget h4, .comment-reply-title, .top-slider-section .post-header h2 {text-transform: none}';
	}
	if (get_theme_mod('p_titles_font_italic')) {
		$styles_output .= '.entry-title,.page-title,.pipdig_p3_related_title a, .entry-content .pipdig_p3_related_posts h3, .pipdig_p3_related_posts h3, .p3_popular_posts_widget h4, .comment-reply-title, .top-slider-section .post-header h2 {font-style:italic}';
	}
	
	
	// Menu font
	$p_navbar_font = get_theme_mod( 'p_navbar_font', 'Oswald' );
	if ($p_navbar_font != 'Oswald') {
		$p_navbar_font = str_replace('+', ' ', $p_navbar_font);
		$styles_output .= '.menu-bar ul li a, .slicknav_menu {letter-spacing: 0; font-family: "'.$p_navbar_font.'"}';
	}
	
	if (get_theme_mod('p_navbar_font_transform', 1) == false) {
		$styles_output .= '.menu-bar ul li a, .slicknav_menu {text-transform: none}';
	}
	if (get_theme_mod('p_navbar_font_italic')) {
		$styles_output .= '.menu-bar ul li a, .slicknav_menu {font-style: italic}';
	}
	
	
	// "You may enjoy" font
	/*
	$pipdig_you_may_enjoy_font = get_theme_mod( 'pipdig_you_may_enjoy_font', 'Oswald' );
	$pipdig_you_may_enjoy_font = str_replace('+', ' ', $pipdig_you_may_enjoy_font);
	if ($pipdig_you_may_enjoy_font != 'Oswald') {
		$styles_output .= '.entry-content .pipdig_p3_related_posts h3, .pipdig_p3_related_posts h3{font-family: "'.$pipdig_you_may_enjoy_font.'"}';
	}
	*/
	
	// post content headings font
	$p_content_heading_font = get_theme_mod( 'p_content_heading_font', 'Oswald' );
	if ($p_content_heading_font != 'Oswald') {
		$p_content_heading_font = str_replace('+', ' ', $p_content_heading_font);
		$styles_output .= '.entry-content h1, .entry-content h2, .entry-content h3, .entry-content h4, .entry-content h5, .entry-content h6 {letter-spacing: 0; font-family: "'.$p_content_heading_font.'"}';
	}
	
	// H2 font size
	$p_content_h2_size = get_theme_mod( 'p_content_h2_size', 17 );
		if ($p_content_h2_size != 17) {
		$styles_output .= '.entry-content h2 {font-size:'.$p_content_h2_size.'px}';
	}
	// H3 font size
	$p_content_h3_size = get_theme_mod( 'p_content_h3_size', 16 );
	if ($p_content_h3_size != 16) {
		$styles_output .= '.entry-content h3 {font-size:'.$p_content_h3_size.'px}';
	}
	// H4 font size
	$p_content_h4_size = get_theme_mod( 'p_content_h4_size', 12 );
	if ($p_content_h4_size != 12) {
		$styles_output .= '.entry-content h4 {font-size:'.$p_content_h4_size.'px}';
	}
	
	if (get_theme_mod('p_content_h_transform', 1) == false) {
		$styles_output .= '.entry-content h1, .entry-content h2, .entry-content h3, .entry-content h4, .entry-content h5, .entry-content h6 {text-transform: none}';
	}
	if (get_theme_mod('p_content_h_italic')) {
		$styles_output .= '.entry-content h1, .entry-content h2, .entry-content h3, .entry-content h4, .entry-content h5, .entry-content h6 {font-style:italic}';
	}
	

	// Show author
	$show_author = get_theme_mod( 'show_author' );
	if ($show_author != 0) {
		$styles_output .= '.show-author{display:inline}';
	}
	
	// Full width layout
	$full_width_layout = get_theme_mod( 'full_width_layout' );
	if ($full_width_layout != 0) {
		$styles_output .= '#pipdig-related-posts li{height:190px}';
	}
	
	// Left sidebar
	$sidebar_position = get_theme_mod( 'sidebar_position' );
	if ($sidebar_position == 'left') {
		$styles_output .= '@media only screen and (min-width: 990px){.site-sidebar{padding:0 30px 0 0;}}';
	}
	
	// Left align menu
	$left_align_menu = get_theme_mod( 'left_align_menu' );
	if ($left_align_menu != 0) {
		$styles_output .= '.menu-bar{text-align:left}.menu-bar ul li a{padding:0 15px}.menu-bar ul li ul a{padding:5px 15px;}';
	}
	
	// Bloglovin widget==========================

	// background color
	$pipdig_bloglovin_widget_background_color = get_theme_mod( 'pipdig_bloglovin_widget_background_color', '#ffffff' ); 
	if ($pipdig_bloglovin_widget_background_color != '#ffffff' ) {
		$styles_output .= '.pipdig-bloglovin-widget{background:'.$pipdig_bloglovin_widget_background_color.'}';
	}
	
	// border color
	$pipdig_bloglovin_widget_border_color = get_theme_mod( 'pipdig_bloglovin_widget_border_color', '#cccccc' ); 
	if ($pipdig_bloglovin_widget_border_color != '#cccccc' ) {
		$styles_output .= '.pipdig-bloglovin-widget{border:1px solid '.$pipdig_bloglovin_widget_border_color.'}';
	}
		
	// text color
	$pipdig_bloglovin_widget_text_color = get_theme_mod( 'pipdig_bloglovin_widget_text_color', '#000000' ); 
	if ($pipdig_bloglovin_widget_text_color != '#000000') {
		$styles_output .= '.pipdig-bloglovin-widget,.wp-bloglovin-widget .fa{color:'.$pipdig_bloglovin_widget_text_color.'!important}';
	}	

	//===========================================
	
	
	// background image and repeat setting (or backstretch)
	$repeat = get_theme_mod('pipdig_background_repeats', 'repeat');
	$bg_image = get_theme_mod('pipdig_background_image');
	if($bg_image) {
		if($repeat != '') { // if not set to backstretch:
			$styles_output .= 'body{background:url('.esc_url($bg_image).') '.$repeat.';}';
		}
	}
	
	// .container max-width and responsive
	$container_width = absint(get_theme_mod( 'container_width', 1080 ));
	if (get_theme_mod('disable_responsive')) {
		$styles_output .= '.container{width:'.$container_width.'px}';
	} else {
		if ($container_width != 1080) {
			$styles_output .= '.container{max-width:'.$container_width.'px}';
		}
	}
	
	// remove padding if using header image
	$logo_image_pad = get_theme_mod( 'logo_image' ); 
	if ($logo_image_pad) {
		$styles_output .= '.site-header .container{padding-top:0;padding-bottom:0;}.site-description{margin-bottom:20px}';
	}

	// Header image top padding
	$header_image_top_padding = get_theme_mod( 'header_image_top_padding', 0 ); 
	if ($header_image_top_padding != 0) {
		$styles_output .= '@media screen and (min-width: 770px) { .site-title img{padding-top:'.$header_image_top_padding.'px} }';
	}
	
	// Header image top padding
	$header_image_bottom_padding = get_theme_mod( 'header_image_bottom_padding', 0 ); 
	if ($header_image_bottom_padding != 0) {
		$styles_output .= '.site-title img{padding-bottom:'.$header_image_bottom_padding.'px}';
	}
	
	// Header image retina
	$header_image_retina_width = get_theme_mod( 'header_image_retina_width', 0 ); 
	if ($header_image_retina_width != 0) {
		$styles_output .= '.site-title img{width:'.$header_image_retina_width.'px}';
	}
	
	// Full width slider height
	$header_full_width_slider_height = get_theme_mod( 'header_full_width_slider_height', 420 ); 
	if ($header_full_width_slider_height != 420) {
		$styles_output .= '@media only screen and (min-width: 720px) {#pipdig_full_width_slider{height:'.$header_full_width_slider_height.'px}}';
	}
	
	// display sorting options on woo pages
	if (!get_theme_mod('pipdig_woo_sorting', 1)) {
		$styles_output .= '.woocommerce-ordering{display:none!important}';
	}
	
	if ($styles_output) {
		echo '<!-- Cust --><style>'.strip_tags($styles_output).'</style><!-- /Cust -->';
	}
	
}
add_action( 'wp_head', 'pipdig_customizer_head_styles' );
