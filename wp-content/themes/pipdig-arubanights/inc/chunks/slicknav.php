<?php
$brand = '';
if (function_exists('p3_slicknav_brand')) {
	$brand = p3_slicknav_brand();
}
if (get_theme_mod('site_top_search')) {
	$brand .= '<a href="#" id="toggle-search-mobile"><i class="fa fa-search"></i></a>';
}
?>
<script>
jQuery(document).ready(function($) {
	$(function(){
		$('.site-menu .menu').slicknav({
			label: '<i class="fa fa-bars"></i>',
			duration: 450,
			brand: '<?php echo $brand; ?>',
			closedSymbol: '<i class="fa fa-chevron-right"></i>',
			openedSymbol: '<i class="fa fa-chevron-down"></i>',
			easingOpen: "swing",
			beforeOpen: function(){
				$('.slicknav_menu .slicknav_menutxt').html('<i class="fa fa-close"></i>');
			},
			beforeClose: function(){
				$('.slicknav_menu .slicknav_menutxt').html('<i class="fa fa-bars"></i>');
			},
		});
	});
	<?php if (get_theme_mod('site_top_search')) { ?>
	$('body').on('click', 'a#toggle-search-mobile', function() {
		$(".slicknav_nav").slideDown();
		$('.pipdig_navbar_search .form-control').focus();
		$('.slicknav_menutxt .fa-bars').addClass('fa-close').removeClass('fa-bars');
	});
	<?php } ?>
});
</script>
