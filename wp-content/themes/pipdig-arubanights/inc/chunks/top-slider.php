<div id="pipdig-top-slider" class="nopin">
	<div id="pipdig-top-slider-content">
	<ul class="bxslider">
	
	<?php
	$post_cat = get_theme_mod('top_slider_post_category');
	$num_posts = absint(get_theme_mod('top_slider_posts', 3));
	$args = array(
		'showposts' => $num_posts,
		'cat' => $post_cat,
	);
	$the_query = new WP_Query( $args );
	while ($the_query -> have_posts()) : $the_query -> the_post();
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		if ($thumb) {
			$bg = esc_url($thumb['0']);
		} else {
			$bg = pipdig_catch_that_image();
		}
	?>
	
		<li>
	
			<div class="top-slider-section" style="background-image:url(<?php echo $bg; ?>);">
			
				<div class="top-slider-overlay">
					<div class="top-slider-overlay-inner">
						
						<div class="post-header">
							
							<h2><a href="<?php echo get_permalink(); ?>"><?php $title = get_the_title(); echo pipdig_truncate($title, 11); ?></a></h2>
						
							<?php if(get_theme_mod('top_slider_cat', 1)) { ?><span class="pipdig-slider-cats">Posted in: <?php echo pipdig_main_cat(false); ?></span><?php } //end if ?>
							
							<a href="<?php the_permalink(); ?>" class="read-more"><?php _e('View Post', 'pipdig-textdomain'); ?></a>
							
						</div>
					
					</div>
				</div>
				
			</div>
		
		</li>
		
		<?php endwhile; ?>
	
	</ul>
	</div>
</div>