<?php

if (get_theme_mod('header_full_width_slider_metaslider') && class_exists('MetaSliderPlugin')) {
	
	if (is_numeric(get_theme_mod('header_full_width_slider_metaslider'))) {
		$metaslider = '[metaslider id='.intval(get_theme_mod('header_full_width_slider_metaslider')).']';
	} else {
		$metaslider = get_theme_mod('header_full_width_slider_metaslider');
	}
	echo do_shortcode($metaslider);
	
} else {

	$animate = 'fade';
	if (get_theme_mod('header_full_width_slider_transition') == '1') {
		$animate = 'scrollHorz';
	}
	?>

	<div id="pipdig_full_width_slider" class="cycle-slideshow" data-cycle-manual-speed="800" data-cycle-slides="li" data-cycle-speed="1000" data-cycle-fx="<?php echo $animate; ?>" data-cycle-delay="<?php echo intval(get_theme_mod('header_full_width_slider_delay', 3000)); ?>" data-cycle-swipe="true" data-cycle-swipe-fx="scrollHorz">

		<?php
			wp_enqueue_script( 'pipdig-cycle' );
			
			$parallax_effect = '';
			if (get_theme_mod('header_full_width_slider_parallax')) {
				$parallax_effect = 'slide-image_parallax';
			}
			
			$post_cat = absint(get_theme_mod('header_full_width_slider_post_category'));
			$num_posts = absint(get_theme_mod('header_full_width_slider_posts', 4));
			$args = array(
				'showposts' => $num_posts,
				'ignore_sticky_posts' => true
			);
			if ($post_cat) {
				$args['cat'] = $post_cat;
			}
			$the_query = new WP_Query( $args );

			while ($the_query -> have_posts()) : $the_query -> the_post();
			
				$images = '';
				if (function_exists('rwmb_meta')) {
					$images = rwmb_meta( 'pipdig_meta_slider_image', 'type=image&size=full' );
				}
				if ($images){ // if an image has been added via meta box
					foreach ( $images as $image )
					{
						$bg = esc_url($image['url']);
					}
				} else { // if no meta box image, use featured as fallback
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
					if ($thumb) {
						$bg = esc_url($thumb['0']);
					} else {
						$bg = pipdig_catch_that_image();
					}
				}
			?>
			<li>
				<div class="slide-image <?php echo $parallax_effect; ?>" style="background-image:url(<?php echo $bg; ?>);">
					<div class="slide-inside">
						<div class="slide-title">
							<div class="slide-container">
								<?php if(get_theme_mod('header_full_width_slider_cat')) { ?><span class="pipdig-cats"><?php echo pipdig_main_cat(false); ?></span> <?php } ?>
								<div class="slide-h2"><a href="<?php the_permalink(); ?>"><?php echo pipdig_truncate(get_the_title(), 10); ?></a></div>
								<?php if(get_theme_mod('header_full_width_slider_show_date')) { ?><span class="pipdig-cats"><?php the_date(); ?></span> <?php } ?>
								<?php if (get_theme_mod('header_full_width_slider_excerpt')) { ?>
									<p><?php echo pipdig_truncate(get_the_excerpt(), 15); ?></p>
								<?php } //end if ?>
								<?php if (get_theme_mod('header_full_width_slider_view_post', 1)) { ?>
									<a href="<?php the_permalink(); ?>" class="read-more"><?php _e('View Post', 'pipdig-textdomain'); ?></a>
								<?php } //end if ?>
							</div>
						</div>
					</div>
				</div>
			</li>
		<?php endwhile; wp_reset_query(); ?>
		
		<div class="cycle-pager"></div>
		
		<div class='cycle-prev'> </div>
		<div class="cycle-next"> </div>
		
	</div>

<?php } ?>