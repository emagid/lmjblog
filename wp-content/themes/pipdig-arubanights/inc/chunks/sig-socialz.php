<?php
$links = get_option('pipdig_links');
$twitter = $links['twitter'];
$instagram = $links['instagram'];
$facebook = $links['facebook'];
$google = $links['google_plus'];
$bloglovin = $links['bloglovin'];
$pinterest = $links['pinterest'];
$youtube = $links['youtube'];
$tumblr = $links['tumblr'];
$linkedin = $links['linkedin'];
$soundcloud = $links['soundcloud'];
$flickr = $links['flickr'];
$snapchat = $links['snapchat'];
//$email = $links['email'];
?>
<h6><?php _e( 'Follow:', 'pipdig-textdomain' ) ?></h6>
<?php if($twitter) { ?><a href="<?php echo esc_url($twitter); ?>" target="_blank" rel="nofollow noopener" aria-label="twitter" title="twitter"><i class="fa fa-twitter"></i></a> <?php } // end if ?>
<?php if($instagram) { ?><a href="<?php echo esc_url($instagram); ?>" target="_blank" rel="nofollow noopener" aria-label="instagram" title="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($facebook) { ?><a href="<?php echo esc_url($facebook); ?>" target="_blank" rel="nofollow noopener" aria-label="facebook" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($google) { ?><a href="<?php echo esc_url($google); ?>" target="_blank" rel="nofollow noopener" aria-label="google plus" title="google plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($bloglovin) { ?><a href="<?php echo esc_url($bloglovin); ?>" target="_blank" rel="nofollow noopener" aria-label="Bloglovin" title="Bloglovin"><i class="fa fa-plus" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($pinterest) { ?><a href="<?php echo esc_url($pinterest); ?>" target="_blank" rel="nofollow noopener" aria-label="pinterest" title="pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($youtube) { ?><a href="<?php echo esc_url($youtube); ?>" target="_blank" rel="nofollow noopener" aria-label="youtube" title="youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($tumblr) { ?><a href="<?php echo esc_url($tumblr); ?>" target="_blank" rel="nofollow noopener" aria-label="tumblr" title="tumblr"><i class="fa fa-tumblr" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($linkedin) { ?><a href="<?php echo esc_url($linkedin); ?>" target="_blank" rel="nofollow noopener" aria-label="linkedin" title="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($soundcloud) { ?><a href="<?php echo esc_url($soundcloud); ?>" target="_blank" rel="nofollow noopener" aria-label="soundcloud" title="soundcloud"><i class="fa fa-soundcloud" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($flickr) { ?><a href="<?php echo esc_url($flickr); ?>" target="_blank" rel="nofollow noopener" aria-label="flickr" title="flickr"><i class="fa fa-flickr" aria-hidden="true"></i></a> <?php } // end if ?>
<?php if($snapchat) { ?><a href="<?php echo esc_url($snapchat); ?>" target="_blank" rel="nofollow noopener" aria-label="snapchat" title="snapchat"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></a> <?php } // end if ?>