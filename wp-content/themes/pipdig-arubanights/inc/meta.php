<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if (!function_exists('pipdig_meta_boxes')) {
	function pipdig_meta_boxes( $meta_boxes ) {
		$prefix = 'pipdig_meta_';

		// "Post Extras" Meta Box
		$meta_boxes[] = array(
			'id'       => 'post_extras',
			'title'    => __('Extra Post Options', 'pipdig-textdomain').' (pipdig)',
			'pages'    => 'post',
			'context'  => 'normal',
			'priority' => 'high',
			'fields' => array(
				array( // sidebar
					'name'  => __('Sidebar position', 'pipdig-textdomain'),
					'id'    => $prefix . 'post_sidebar_position',
					'type'  => 'image_select',
					'options' => array(
						'sidebar_right' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAAB6AgMAAAANN693AAAADFBMVEXn6Oj////t7u7z9PQ7CQrXAAAAUUlEQVRIx2MIxQsCCMrH/8cHAhgCGPCBUflR+VH5oSwftQofCKCwfMFu/nU4C7v7psFZo/Kj8qPyo/Kj8qPyo/Kj8qPyI00eW//0OpxFcfsUAPRDFitp8MRMAAAAAElFTkSuQmCC',
						'sidebar_left'  => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAAB6AgMAAAANN693AAAADFBMVEXn6Oj////t7u7z9PQ7CQrXAAAAVUlEQVRIx2MIxQsCCMrH/8cHAhgCGPCBUflR+VH5oSwftQofCKC0fEGYv7YKm/kI93EmYHHfqPyo/Kj8qPyo/Kj8qPyo/Kj8SJOH90//1v/HBBS3TwFEJQc6A3xK1gAAAABJRU5ErkJggg==',
						'sidebar_none'  => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAAB6AgMAAAANN693AAAADFBMVEXn6Oj////z9PTt7u5M1tw/AAAAQUlEQVRIx+3WoREAIBADwZQYg8HQH01CB0G8YGDu7Ko3P1GL+eh9piwrheP4yz5WysX/cv8+HMdxHMdx/C8v7tMNa4G2Y2L7ihAAAAAASUVORK5CYII=',
					),
				),
				array( // post location
					'name'  => __('Post Location', 'pipdig-textdomain'),
					'desc'  => __('This will be shown in the footer of the post and link to a Google Map.', 'pipdig-textdomain'),
					'id'    => $prefix . 'geographic_location',
					'placeholder' => __('e.g. London', 'pipdig-textdomain'),
					'type'  => 'text',
				),
				array( // post excerpt extra
					'name'  => __('Extra Post Excerpt ("Excerpt/Summary" Post Layout only)', 'pipdig-textdomain'),
					'desc'  => 'You can use this field to display extra information when using the "Excerpt/Summary" post layout in the <a href="'.admin_url('customize.php?autofocus[section]=post_layouts').'" target="_blank">Customizer</a>.<br />For example, you may wish to display a <a href="https://support.pipdig.co/articles/blogger-how-do-i-setup-a-shop-the-post-feature-or-ecommerce/" target="_blank">Shop the Post widget</a> on your homepage above the "View Post" button. This field can include HTML/Javascript or Shortcodes.',
					'id'    => $prefix . 'extra_excerpt',
					'type'  => 'textarea',
				),
				array(
				'name'  => __('Disclaimer to display in footer', 'pipdig-textdomain'),
				'id'    => $prefix . 'post_disclaimer',
				'type'  => 'wysiwyg',
				'raw'	=> true,
				'options' => array(
					'media_buttons' => false,
					'dfw'  => false,
					'textarea_rows'  => 2,
					),
				),
			)
		);
		
		return $meta_boxes;
	}
	add_filter( 'rwmb_meta_boxes', 'pipdig_meta_boxes' );
}


// image gallery meta
if (!function_exists('pipdig_meta_boxes_pages')) {
	function pipdig_meta_boxes_pages( $meta_boxes ) {
		$prefix = 'pipdig_meta_';

		// "Post Extras" Meta Box
		$meta_boxes[] = array(
			'id'		=> 'page_extras',
			'title'		=> 'Image Gallery',
			'pages'		=> 'page',
			'include'	=> array(
				'template' => 'template-gallery.php',
			),
			'context'	=> 'normal',
			'priority'	=> 'high',
			'fields'	=> array(
				array(
					'name'  => 'Gallery images for this page',
					//'desc'  => '',
					'id'    => $prefix . 'gallery_images',
					'type'  => 'image_advanced',
					//'max_file_uploads' => 1,
				),
			)
		);
		
		return $meta_boxes;
	}
	add_filter( 'rwmb_meta_boxes', 'pipdig_meta_boxes_pages' );
}
