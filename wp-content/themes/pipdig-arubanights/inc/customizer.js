(function($) {
	
	// Main text Color
	wp.customize('p_main_text_color', function(value) {
		value.bind(function(newval) {
			$('body, .entry-meta, .entry-footer a, .entry-meta a:hover, .entry-meta a:focus, .addthis_toolbox .fa, .page-numbers a, .page-numbers span').css('color', newval);
		});
	});
	
	// Header Text Color
	wp.customize('header_color', function(value) {
		value.bind(function(newval) {
			$('.site-title a').css('color', newval);
		});
	});
	

	// Navbar Background Color
	wp.customize('navbar_background_color', function(value) {
		value.bind(function(newval) {
			$('.site-top').css('background', newval).css('border', '0');
		});
	});
	
	// Widget Title Text Color
	wp.customize('pipdig_widget_title_text_color', function(value) {
		value.bind(function(newval) {
			$('.widget-title').css('color', newval);
		});
	});
	
	// Widget Title Background Color
	wp.customize('pipdig_widget_title_background_color', function(value) {
		value.bind(function(newval) {
			$('.widget-title').css('background', newval).css('border', '0').css('margin-bottom', '15px');
		});
	});

	// Social Icon Color
	wp.customize('social_color', function(value) {
		value.bind(function(newval) {
			$('.pipdig_widget_social_icons .socialz a').css('color', newval);
		});
	});

	// Post content link color
	wp.customize('post_links_color', function(value) {
		value.bind(function(newval) {
			$('.entry-content a').css('color', newval);
		});
	});
	
	// Footer bar Text Color
	wp.customize('footer_bar_text_color', function(value) {
		value.bind(function(newval) {
			$('.site-footer,.site-footer a,.site-footer a:hover').css('color', newval);
		});
	});
	
	// Footer bar Background Color
	wp.customize('footer_bar_bg_color', function(value) {
		value.bind(function(newval) {
			$('.site-footer').css('background', newval);
		});
	});

})(jQuery);