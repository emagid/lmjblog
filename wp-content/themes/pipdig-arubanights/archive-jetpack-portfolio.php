<?php get_header(); ?>

	<div class="row">
	
		<?php do_action('p3_main_row_start'); ?>
	
		<div class="content-area">
		
			<h1 class="entry-title p_post_titles_font"><?php _e('Portfolio', 'pipdig-textdomain'); ?></h1>

		<?php if ( have_posts() ) { ?>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'p3_medium' );
				if ($thumb) {
					$bg = esc_url($thumb['0']);
				} else {
					$bg = pipdig_catch_that_image();
				}
				?>
				<div class="pipdig_portfolio_grid_item">
					<a href="<?php the_permalink(); ?>" class="p3_cover_me" style="background-image:url(<?php echo $bg; ?>);">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAAHgAQMAAACyyGUjAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAACxJREFUeNrtwTEBAAAAwiD7p7bGDmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkHVZAAAFam5MDAAAAAElFTkSuQmCC" alt="" class="p3_invisible" data-pin-nopin="true"/>
							<div class="pipdig_portfolio_grid_title_box">
							<h2 class="title"><?php the_title(); ?></h2>
							<div class="read_more"><?php _e('Click to view', 'pipdig-textdomain'); ?></div>
							</div>
						</a>
					</div>
			<?php endwhile; ?>
					
			<div class="clearfix"></div>
			
			<nav id="mosaic-nav" role="navigation">
			
			<?php if (get_next_posts_link()) { ?>
				<div class="nav-previous"><?php echo get_next_posts_link( '<span class="meta-nav"><i class="fa fa-chevron-left"></i></span> '.__( 'Older projects', 'p3' ), $wp_query->max_num_pages ); ?></div>
			<?php } ?>

			<?php if (get_previous_posts_link()) { ?>
				<div class="nav-next"><?php echo get_previous_posts_link(__( 'Newer projects', 'p3' ).' <span class="meta-nav"><i class="fa fa-chevron-right"></i></span>'); ?></div>
			<?php } ?>
			
			</nav>

		<?php } else { ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php } //endif ?>

		</div><!-- .content-area -->

		<?php //get_sidebar(); ?>

	</div>

<?php get_footer(); ?>
