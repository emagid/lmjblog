<?php
if (!defined('ABSPATH')) die;
get_header();

$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
?>

	<div class="row">
	
		<div id="content" class="col-xs-12 content-area">
		
		<div class="col-xs-12 entry-content" style="text-align: center">
		
			<?php echo get_avatar($curauth->ID, 200); ?>
			
			<h1 class="entry-title"><?php echo esc_html($curauth->nickname); ?></h1>
			
			<?php if ($curauth->description) { ?>
				<p><?php echo wpautop(wp_kses_post($curauth->description)); ?></p>
			<?php } ?>
			
			<h2>Posts by this author:</h2>
			
		</div>
		

		<?php if ( have_posts() ) { ?>
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part('content-grid', get_post_format()); ?>
			<?php endwhile; ?>

			<div class="clearfix"></div>
			<div class="next-prev-hider"><?php pipdig_content_nav('nav-below'); ?></div>
			<?php pipdig_pagination(); ?>

		<?php } else { ?>

			<p>There are no posts by this author</p>

		<?php } ?>

		</div><!-- .content-area -->

	</div>

<?php get_footer(); ?>
