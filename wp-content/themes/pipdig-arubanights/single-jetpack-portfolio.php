<?php get_header(); ?>

	<div class="row">
	
		<div class="<?php pipdig_left_or_right(); ?> content-area">
		
				<header class="entry-header">
					<h1 class="entry-title p_post_titles_font"><?php the_title(); ?></h1>
				</header>

				<div class="clearfix entry-content">
					<div class="textalign-center">
						<?php
							$link = esc_url(get_the_permalink());
							$title = rawurldecode(get_the_title());
							if (get_the_post_thumbnail() != '') {
								$attr = array(
									'data-p3-pin-title' => $title,
									'data-p3-pin-link' => $link,
								);
								the_post_thumbnail('full', $attr);
							} 
						?>
					</div>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>

			<?php //pipdig_content_nav( 'nav-below' ); ?>

			<?php 
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
			?>

		</div><!-- .content-area -->

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>