<?php
$disclaimer = $post_location = '';
if (function_exists('rwmb_meta')) {
	$post_location = rwmb_meta( 'pipdig_meta_geographic_location' );
	$disclaimer = rwmb_meta( 'pipdig_meta_post_disclaimer' );
}

$sig_image = get_theme_mod('post_signature_image');
$sig_icons = get_theme_mod('show_socialz_signature');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
	
	<header class="entry-header">
		<?php if ( 'post' == get_post_type() ) { ?>
			<div class="entry-meta">
				<span class="date-bar-white-bg">
				
					<span class="vcard author show-author">
						<span class="fn">
							<?php the_author_posts_link(); ?>
						</span>
						<span class="show-author"></span>
					</span>
					
					<?php if (!get_theme_mod('hide_dates')) { ?>
						<span class="entry-date updated">
							<time datetime="<?php echo get_the_date('Y-m'); ?>"><?php echo get_the_date(); ?></time>
						</span>
					<?php } ?>
					
					<?php if (get_theme_mod('show_main_cat')) { ?>
						<span class="main_cat"><?php echo pipdig_main_cat(); ?></span>
					<?php } ?>
					
					<?php if ($post_location) { ?>
						<span class="p_post_location"><i class="fa fa-map-marker"></i><?php echo strip_tags($post_location); ?></span>
					<?php } ?>
					
				</span>
			</div>
		<?php } ?>
		<?php the_title('<h1 class="entry-title p_post_titles_font">', '</h1>'); ?>
	</header><!-- .entry-header -->

	<div class="clearfix entry-content">
	
		<?php do_action('p3_content_start'); ?>
	
		<?php the_content(); ?>
		
		<?php if ($sig_icons || $sig_image){ ?>
		<div class="pipdig-post-sig socialz nopin">
			<?php if ($sig_image) { ?>
				<img src="<?php echo esc_url($sig_image); ?>" data-pin-nopin="true" alt="" />
			<?php } ?>
			<?php if ($sig_icons){ ?>
				<?php get_template_part('inc/chunks/sig-socialz'); ?>
			<?php } ?>
		</div>
		<?php } ?>
		
		<?php do_action('p3_content_end'); ?>
		
	</div>

	<footer class="entry-meta entry-footer">
		<?php if ( 'post' == get_post_type() ) : ?>

			<?php if (function_exists('pipdig_p3_social_shares')){ pipdig_p3_social_shares(); } ?>

			<?php if (!get_theme_mod('hide_post_tags')){ ?>
				<span class="tags-links">
					<?php if (get_the_tag_list()) {
						echo get_the_tag_list('<i class="fa fa-tags"></i> ',', ','');
					} ?>
				</span>
			<?php } ?>
			
			<?php if ($disclaimer) { ?>
				<div class="disclaimer-text">
					<i class="fa fa-info-circle"></i> <?php echo wp_kses_post($disclaimer); ?>
				</div>
			<?php } ?>
				
			<?php if ($post_location) { ?>
			<br />
			<div class="location">
				<a href="http://maps.google.com/?q=<?php echo $post_location ?>" target="_blank" rel="nofollow"><i class="fa fa-map-marker"></i>
				<?php echo $post_location ?>
				</a>
			</div>
			<?php } ?>
			
		<?php endif; ?>
	</footer>

<!-- #post-<?php the_ID(); ?> --></article>