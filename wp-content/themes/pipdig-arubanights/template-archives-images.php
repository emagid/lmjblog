<?php
/**
 * Template Name: Archives with Images
 */
get_header(); ?>

	<div class="row">
	
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

		<?php endwhile; ?>
	
		<?php
		
			if ( false === ( $all_posts = get_transient( 'pipdig_archives_page' ) ) ) {
				$all_posts = get_posts(array(
				  'posts_per_page' => 120,
				  'suppress_filters' => 0
				));
				set_transient( 'pipdig_archives_page', $all_posts, 24 * HOUR_IN_SECONDS );
			}
			
			$shape = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0AQMAAADxGE3JAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAADVJREFUeNrtwTEBAAAAwiD7p/ZZDGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOX0AAAEidG8rAAAAAElFTkSuQmCC'; // square
			$x = 1;
			foreach ($all_posts as $post) {
				$bg = pipdig_catch_that_image($post->ID, 'p3_medium', 'pipdig_meta_post_image_home');
				?>
				<div class="col-xs-12 col-md-3" style="margin-bottom: 20px;">
					<?php if (($x > 12) && get_theme_mod('pipdig_lazy')) { ?>
						<a class="p3_cover_me nopin pipdig_lazy" data-src="<?php echo $bg; ?>" href="<?php the_permalink(); ?>">
					<?php } else { ?>
						<a class="p3_cover_me nopin" style="background-image:url(<?php echo $bg; ?>);" href="<?php the_permalink(); ?>">
					<?php } ?>
						<img src="<?php echo $shape; ?>" alt="<?php the_title_attribute(); ?>" class="p3_invisible" />
					</a>
					<?php the_title('<p class="p3_post_archives_titles" style="margin-top: 6px;">', '<p>'); ?>
				</div>
				<?php if ($x % 4 == 0) { ?>
					<div class="clearfix"></div>
				<?php } ?>
				<?php
				$x++;
			}

		?>
		
	</div>

<?php get_footer(); ?>