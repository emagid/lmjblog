<?php if (!defined('ABSPATH')) die;
get_header(); ?>

	<div class="row">

		<div class="<?php pipdig_left_or_right(); ?> content-area">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( "Sorry, We can't find that page!", 'pipdig-textdomain' ); ?></h1>
				</header>

				<div class="page-content">
					<p><?php _e( 'It looks like this page is missing. Maybe try a search below?', 'pipdig-textdomain' ); ?></p>

					<?php get_search_form(); ?>

				</div>
			</section>
		</div><!-- .content-area -->

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>