<?php
$home_layout = absint(get_theme_mod('home_layout', 1));
$category_layout = absint(get_theme_mod('category_layout', 1));
$this_post = $wp_query->current_post;

$disclaimer = $post_location = $extra_excerpt = '';
if (function_exists('rwmb_meta')) {
	$post_location = rwmb_meta( 'pipdig_meta_geographic_location' );
	$disclaimer = rwmb_meta( 'pipdig_meta_post_disclaimer' );
	$extra_excerpt = rwmb_meta( 'pipdig_meta_extra_excerpt' );
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>

	<header class="entry-header">
		<?php if ( 'post' == get_post_type() ) { ?>
			<div class="entry-meta">
				<span class="date-bar-white-bg">
				
					<span class="vcard author show-author">
						<span class="fn">
							<?php the_author_posts_link(); ?>
						</span>
						<span class="show-author"></span>
					</span>
					
					<?php if (!get_theme_mod('hide_dates')) { ?>
						<span class="entry-date updated">
							<time datetime="<?php echo get_the_date('Y-m'); ?>"><?php echo get_the_date(); ?></time>
						</span>
					<?php } ?>
					
					<?php if (get_theme_mod('show_main_cat')) { ?>
						<span class="main_cat"><?php echo pipdig_main_cat(); ?></span>
					<?php } ?>
					
					<?php if ($post_location) { ?>
						<span class="p_post_location"><i class="fa fa-map-marker"></i><?php echo strip_tags($post_location); ?></span>
					<?php } ?>
					
				</span>
			</div>
		<?php } ?>
		<h2 class="entry-title p_post_titles_font"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header><!-- .entry-header -->

	<?php if ( ((is_home() && ($home_layout == 2))) || ((is_archive() || is_search()) && ($category_layout == 2)) || (is_home() && ($home_layout == 5) && $wp_query->current_post == 0) || ((is_archive() || is_search()) && ($category_layout == 5) && $wp_query->current_post == 0) ) { ?>
		<div class="entry-summary">
		
			<?php do_action('p3_summary_start'); ?>
		
			<div class="textalign-center">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
					<?php
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
					if ($thumb) {
						$bg = esc_url($thumb['0']);
					} else {
						$bg = pipdig_catch_that_image();
					}
					if ($wp_query->current_post != 0 && get_theme_mod('pipdig_lazy')) {
						echo '<img class="pipdig_lazy" data-src="'.$bg.'" data-p3-pin-title="'.rawurldecode(get_the_title()).'" data-p3-pin-link="'.esc_url(get_the_permalink()).'" alt="'.esc_attr(get_the_title()).'" />';
					} else {
						echo '<img src="'.$bg.'" data-p3-pin-title="'.rawurldecode(get_the_title()).'" data-p3-pin-link="'.esc_url(get_the_permalink()).'" alt="'.esc_attr(get_the_title()).'" />';
					}
					?>
				</a>
			</div>
			
			<?php
			if (get_theme_mod('excerpt_length_num', 35) !== 0) {
				the_excerpt();
			}
			?>
			
			<?php
			if ($extra_excerpt) {
				echo '<p style="text-align: center">';
				echo do_shortcode($extra_excerpt);
				echo '</p>';
			}
			?>
			
			<a class="more-link" href="<?php the_permalink(); ?>"><?php _e('View Post', 'pipdig-textdomain'); ?></a>
			
			<?php do_action('p3_summary_end'); ?>
		
		</div><!-- .entry-summary -->
		
	<?php } else { ?>
		<div class="clearfix entry-content">
			
			<?php do_action('p3_content_start'); ?>
			
			<?php the_content( __( 'View Post', 'pipdig-textdomain' ) ); ?>
			
			<div class="pipdig-post-sig socialz nopin">
				<?php if(get_theme_mod('post_signature_image')) { ?>
					<img src="<?php echo esc_url(get_theme_mod('post_signature_image')); ?>" data-pin-nopin="true" />
				<?php } //endif ?>
				<?php if(true == get_theme_mod('show_socialz_signature')){ ?>
						<?php get_template_part('inc/chunks/sig-socialz'); ?>
				<?php } // end if ?>
			</div>
			
			<?php if ($disclaimer) { ?>
				<div class="disclaimer-text">
					<i class="fa fa-info-circle"></i> <?php echo wp_kses_post($disclaimer); ?>
				</div>
			<?php } ?>
			
			<?php do_action('p3_content_end'); ?>
			
		</div><!-- .entry-content -->
	<?php } ?>

	<footer class="entry-meta entry-footer">
		<?php if ( 'post' == get_post_type() ) { ?>

			<?php if(function_exists('pipdig_p3_social_shares')){ pipdig_p3_social_shares(); } ?>
			
			<?php if(!get_theme_mod('hide_comments_link')){ ?>
				<span class="commentz"><a href="<?php comments_link(); ?>" data-disqus-url="<?php echo esc_url(get_the_permalink()); ?>"><?php if (function_exists('pipdig_p3_comment_count')) { pipdig_p3_comment_count(); } else { ?>Comments<?php } ?></a></span>
			<?php } // end if ?>
			
		<?php } //endif ?>

	</footer><!-- .entry-footer -->
<!-- #post-<?php the_ID(); ?> --></article>

<?php
if ($this_post == 0) {
	do_action( 'after_first_post' );
	if ( is_active_sidebar('after-first-post') && is_home() && !is_paged() ) { ?>
		<div id="widget_area_after_first" class="p_after_post_widget_section textalign-center clearfix">
			<?php dynamic_sidebar( 'after-first-post' ); ?>
		</div> <?php
	}
} elseif ($this_post == 1) {
	do_action( 'after_second_post' );
	if ( is_active_sidebar('after-second-post') && is_home() && !is_paged() ) { ?>
		<div id="widget_area_after_second" class="p_after_post_widget_section textalign-center clearfix">
			<?php dynamic_sidebar( 'after-second-post' ); ?>
		</div> <?php
	}
} elseif ($this_post == 2) {
	do_action( 'after_third_post' );
	if ( is_active_sidebar('after-third-post') && is_home() && !is_paged() ) { ?>
		<div id="widget_area_after_third" class="p_after_post_widget_section textalign-center clearfix">
			<?php dynamic_sidebar( 'after-third-post' ); ?>
		</div> <?php
	}
} elseif ($this_post == 3) {
	if ( is_active_sidebar('after-fourth-post') && is_home() && !is_paged() ) { ?>
		<div id="widget_area_after_fourth" class="p_after_post_widget_section textalign-center clearfix">
			<?php dynamic_sidebar( 'after-fourth-post' ); ?>
		</div> <?php
	}
}
?>