msgid ""
msgstr ""
"Project-Id-Version: pipdig Theme\n"
"POT-Creation-Date: 2017-09-21 14:27+0100\n"
"PO-Revision-Date: 2017-09-21 14:27+0100\n"
"Last-Translator: pipdig <hello@pipdig.co>\n"
"Language-Team: pipdig\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.3\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: .git\n"
"X-Poedit-SearchPathExcluded-1: css\n"
"X-Poedit-SearchPathExcluded-2: js\n"
"X-Poedit-SearchPathExcluded-3: inc/plugins\n"

#: 404.php:9 content-none.php:22
msgid "Sorry, We can't find that page!"
msgstr ""

#: 404.php:13
msgid "It looks like this page is missing. Maybe try a search below?"
msgstr ""

#: archive-jetpack-portfolio.php:9
msgid "Portfolio"
msgstr ""

#: archive-jetpack-portfolio.php:27 taxonomy-jetpack-portfolio-type.php:32
msgid "Click to view"
msgstr ""

#: archive-jetpack-portfolio.php:38
msgid "Older projects"
msgstr ""

#: archive-jetpack-portfolio.php:42
msgid "Newer projects"
msgstr ""

#: content-grid.php:73 content.php:49 content.php:60 functions.php:533
#: inc/chunks/layerslider-full-width.php:44 inc/chunks/top-slider.php:35
msgid "View Post"
msgstr ""

#: content-grid.php:82 content.php:91
msgid "Comments"
msgstr ""

#: content-none.php:3
msgid "Nothing Found"
msgstr ""

#: content-none.php:9
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content-none.php:13
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: footer.php:20
msgid "Where to next?"
msgstr ""

#: functions.php:141
msgid "Primary Menu"
msgstr ""

#: functions.php:142
msgid "Footer"
msgstr ""

#: functions.php:225
msgid ""
"You will need to enable the sidebar in the Customizer before adding widgets "
"to this section."
msgstr ""

#: functions.php:229
msgid "Sidebar"
msgstr ""

#: functions.php:238 functions.php:246 functions.php:254 functions.php:262
msgid "Footer Column"
msgstr ""

#: functions.php:270
msgid "Header Full Width"
msgstr ""

#: functions.php:278
msgid "Footer Full Width"
msgstr ""

#: functions.php:494 inc/template-tags.php:79
msgid "Newer Posts"
msgstr ""

#: functions.php:495 inc/template-tags.php:75
msgid "Older Posts"
msgstr ""

#: inc/chunks/layerslider-full-width.php:42
msgid "Posted in:"
msgstr ""

#: inc/chunks/scotch.php:3
msgid "Close Me"
msgstr ""

#: inc/chunks/scotch.php:4
msgid "Looking for Something?"
msgstr ""

#: inc/chunks/scotch.php:6
msgid "Search:"
msgstr ""

#: inc/chunks/scotch.php:10
msgid "Product Categories:"
msgstr ""

#: inc/chunks/scotch.php:14
msgid "Post Categories:"
msgstr ""

#: inc/chunks/sig-socialz.php:17
msgid "Follow:"
msgstr ""

#: inc/customizer.php:88
msgid "Header Options"
msgstr ""

#: inc/customizer.php:96
msgid "General Options"
msgstr ""

#: inc/customizer.php:104
msgid "Layout Options"
msgstr ""

#: inc/customizer.php:113
msgid "Post Layout Options"
msgstr ""

#: inc/customizer.php:114
msgid ""
"Use these settings to select different post layouts for your blog. You can "
"also change the number of posts displayed by going to 'Settings > Reading' "
"in your dashboard."
msgstr ""

#: inc/customizer.php:122
msgid "Color Options"
msgstr ""

#: inc/customizer.php:129
msgid "Background Image"
msgstr ""

#: inc/customizer.php:130
msgid ""
"Use these options to add a background image to your site. Note, if you would "
"like a solid color please use the options in the \"Color Options\" section "
"above."
msgstr ""

#: inc/customizer.php:137
msgid "Font Options"
msgstr ""

#: inc/customizer.php:138
msgid ""
"We have selected some of our favorite fonts from Google Fonts.  If you would "
"like even more font options, please see <a href=\"http://support.pipdig.co/"
"articles/wordpress-how-do-i-change-the-fonts/\" target=\"_blank\">this "
"guide</a>."
msgstr ""

#: inc/customizer.php:146
msgid "Post Category Options"
msgstr ""

#: inc/customizer.php:147
msgid "These options will apply to post categories."
msgstr ""

#: inc/customizer.php:155
msgid "Blog Post Options"
msgstr ""

#: inc/customizer.php:164
msgid "Woocommerce Options"
msgstr ""

#: inc/customizer.php:177
msgid "Full Width Slider"
msgstr ""

#: inc/customizer.php:178
msgid ""
"Use these options to display a full width post slider under the header. "
"NOTE: for a post to be included in the slider it needs to have a \"Featured "
"Image\" set."
msgstr ""

#: inc/customizer.php:185
msgid "Large Feature Slider"
msgstr ""

#: inc/customizer.php:186
msgid ""
"Use these options to display a large square post slider under the header. "
"NOTE: for a post to be included in the slider it needs to have a \"Featured "
"Image\" set."
msgstr ""

#: inc/customizer.php:194
msgid "Posts Carousel"
msgstr ""

#: inc/customizer.php:195
msgid ""
"Use these options to display a moving carousel of posts across the bottom of "
"your site."
msgstr ""

#: inc/customizer.php:205
msgid "Bloglovin' Widget"
msgstr ""

#: inc/customizer.php:206
msgid "Use these settings to style our custom Bloglovin' Widget."
msgstr ""

#: inc/customizer.php:227
msgid "Remove the tagline from header"
msgstr ""

#: inc/customizer.php:243
msgid "Upload a header image"
msgstr ""

#: inc/customizer.php:261
msgid "Header image spacing (top)"
msgstr ""

#: inc/customizer.php:281
msgid "Header image spacing (bottom)"
msgstr ""

#: inc/customizer.php:301
msgid "Header image width (pixels)"
msgstr ""

#: inc/customizer.php:302
msgid ""
"This option is not required, but can be useful if you want to force a "
"specific size, or if you want to make the image appear more clear on retina "
"screens. If you are unsure what this means, it is recommended that you do "
"not edit this option."
msgstr ""

#: inc/customizer.php:326
msgid "Enable full width slider"
msgstr ""

#: inc/customizer.php:342
msgid "Display on home page only"
msgstr ""

#: inc/customizer.php:357 inc/customizer.php:511
msgid "Number of posts to show in the slider"
msgstr ""

#: inc/customizer.php:381
msgid "How long should each slide be displayed?"
msgstr ""

#: inc/customizer.php:384 inc/customizer.php:385 inc/customizer.php:386
#: inc/customizer.php:387 inc/customizer.php:388 inc/customizer.php:389
msgid "seconds"
msgstr ""

#: inc/customizer.php:405
msgid "Slider animation style"
msgstr ""

#: inc/customizer.php:408
msgid "Fade"
msgstr ""

#: inc/customizer.php:409
msgid "Slide"
msgstr ""

#: inc/customizer.php:427 inc/customizer.php:537
msgid "Only display posts from:"
msgstr ""

#: inc/customizer.php:445 inc/customizer.php:555
msgid "Display post category"
msgstr ""

#: inc/customizer.php:461
msgid "Include post excerpt"
msgstr ""

#: inc/customizer.php:462
msgid "Display the excerpt from the post."
msgstr ""

#: inc/customizer.php:476
msgid "Meta Slider ID (optional)"
msgstr ""

#: inc/customizer.php:495
msgid "Enable this slider"
msgstr ""

#: inc/customizer.php:496
msgid ""
"Displays the Featured Image from your 4 most recent posts in a large slider "
"at the top of the home page"
msgstr ""

#: inc/customizer.php:576
msgid "Main Width"
msgstr ""

#: inc/customizer.php:577
msgid "The number below is how wide your website is in pixels:"
msgstr ""

#: inc/customizer.php:597
msgid "Disable Responsive layout"
msgstr ""

#: inc/customizer.php:598
msgid ""
"This theme automatically adapts to different screen sizes. Select this "
"option to disable this, and show the same thing on all devices (not "
"recommended)."
msgstr ""

#: inc/customizer.php:617
msgid "Sidebar position:"
msgstr ""

#: inc/customizer.php:619 inc/customizer.php:1737
msgid "Left"
msgstr ""

#: inc/customizer.php:620 inc/customizer.php:1738
msgid "Right"
msgstr ""

#: inc/customizer.php:636
msgid "Completely disable the sidebar"
msgstr ""

#: inc/customizer.php:637
msgid ""
"Removes the sidebar from all pages and posts. This will override any "
"settings below."
msgstr ""

#: inc/customizer.php:653
msgid "Disable sidebar on home/blog"
msgstr ""

#: inc/customizer.php:669
msgid "Disable sidebar on categories/archives"
msgstr ""

#: inc/customizer.php:685
msgid "Disable sidebar on all posts"
msgstr ""

#: inc/customizer.php:702
msgid "Disable sidebar on all pages"
msgstr ""

#: inc/customizer.php:720
msgid "Disable sidebar on WooCommerce"
msgstr ""

#: inc/customizer.php:739
msgid "Homepage layout"
msgstr ""

#: inc/customizer.php:740
msgid "This setting will effect posts displayed on the homepage."
msgstr ""

#: inc/customizer.php:743 inc/customizer.php:767
msgid "Full blog posts"
msgstr ""

#: inc/customizer.php:744
msgid "Excerpt/Summary"
msgstr ""

#: inc/customizer.php:745 inc/customizer.php:769
msgid "1st post Full, then grid for others"
msgstr ""

#: inc/customizer.php:746 inc/customizer.php:770
msgid "1st post Excerpt, then grid for others"
msgstr ""

#: inc/customizer.php:747 inc/customizer.php:771
msgid "Grid"
msgstr ""

#: inc/customizer.php:763
msgid "Post Categories layout"
msgstr ""

#: inc/customizer.php:764
msgid "This setting will effect posts displayed in categories."
msgstr ""

#: inc/customizer.php:768
msgid "Excerpt/Summary only"
msgstr ""

#: inc/customizer.php:786
msgid "Excerpt length (words):"
msgstr ""

#: inc/customizer.php:787
msgid "This setting will effect all posts when using an excerpt based layout."
msgstr ""

#: inc/customizer.php:809
msgid "Show post excerpt in grid layout"
msgstr ""

#: inc/customizer.php:810
msgid "Select this option to display the post excerpt in any grid based layout"
msgstr ""

#: inc/customizer.php:830
msgid "Show author name on posts"
msgstr ""

#: inc/customizer.php:831
msgid "Shows the author's name next to the date on each post header"
msgstr ""

#: inc/customizer.php:847
msgid "Display Social Media follow section"
msgstr ""

#: inc/customizer.php:848
msgid ""
"This option will add links to your social media pages at the end of each "
"post."
msgstr ""

#: inc/customizer.php:864
msgid "Post signature image"
msgstr ""

#: inc/customizer.php:865
msgid "This image will be shown in the footer of your posts"
msgstr ""

#: inc/customizer.php:882
msgid "Remove link to comments"
msgstr ""

#: inc/customizer.php:883
msgid ""
"This option will remove the \"No comments\" or \"x Comments\" link from "
"posts on the home page"
msgstr ""

#: inc/customizer.php:898
msgid "Remove tags from posts"
msgstr ""

#: inc/customizer.php:899
msgid ""
"Select this option if you would prefer for your tags not to be shown at the "
"bottom of posts"
msgstr ""

#: inc/customizer.php:920
msgid "Enable post carousel in footer"
msgstr ""

#: inc/customizer.php:921
msgid ""
"This will display 15 random posts in the footer of your site. Select a date "
"range from below for posts to include."
msgstr ""

#: inc/customizer.php:936
msgid "Date range for posts:"
msgstr ""

#: inc/customizer.php:939 inc/customizer.php:1748
msgid "1 Year"
msgstr ""

#: inc/customizer.php:940 inc/customizer.php:1749
msgid "1 Month"
msgstr ""

#: inc/customizer.php:941 inc/customizer.php:1750
msgid "1 Week"
msgstr ""

#: inc/customizer.php:942 inc/customizer.php:1751
msgid "All Time"
msgstr ""

#: inc/customizer.php:963
msgid "Search button"
msgstr ""

#: inc/customizer.php:980
msgid "Display social counter in the Footer"
msgstr ""

#: inc/customizer.php:981
msgid ""
"Select this option to show your social media follower counts at the bottom "
"of your website. Yuo will need to add your links to \"pipdig > Social Links"
"\" first."
msgstr ""

#: inc/customizer.php:997
msgid "Left-align the navbar"
msgstr ""

#: inc/customizer.php:998
msgid ""
"Select this option to align the navbar menu items to the left, rather than "
"in the center."
msgstr ""

#: inc/customizer.php:1013
msgid "Copyright text in footer:"
msgstr ""

#: inc/customizer.php:1037
msgid "Upload image"
msgstr ""

#: inc/customizer.php:1054
msgid "Background image layout:"
msgstr ""

#: inc/customizer.php:1057 inc/customizer.php:1724
msgid "Tile"
msgstr ""

#: inc/customizer.php:1058 inc/customizer.php:1725
msgid "Tile Horizontally"
msgstr ""

#: inc/customizer.php:1059 inc/customizer.php:1726
msgid "Tile Vertically"
msgstr ""

#: inc/customizer.php:1060 inc/customizer.php:1727
msgid "Stretch Full Screen"
msgstr ""

#: inc/customizer.php:1083
msgid "Number of Products displayed in the shop:"
msgstr ""

#: inc/customizer.php:1102
msgid "Remove sharing icons from products"
msgstr ""

#: inc/customizer.php:1122
msgid "Background color"
msgstr ""

#: inc/customizer.php:1139
msgid "Border color"
msgstr ""

#: inc/customizer.php:1156
msgid "Text color"
msgstr ""

#: inc/customizer.php:1173
msgid "Widget Icon"
msgstr ""

#: inc/customizer.php:1176
msgid "Heart"
msgstr ""

#: inc/customizer.php:1177
msgid "Plus"
msgstr ""

#: inc/customizer.php:1178
msgid "None"
msgstr ""

#: inc/customizer.php:1195
msgid "Use the official Bloglovin' widget"
msgstr ""

#: inc/customizer.php:1196
msgid ""
"Select this option if you would prefer to use the official Bloglovin' widget."
msgstr ""

#: inc/customizer.php:1214
msgid "Navbar background color"
msgstr ""

#: inc/customizer.php:1230
msgid "Navbar text color"
msgstr ""

#: inc/customizer.php:1246
msgid "Navbar text hover color"
msgstr ""

#: inc/customizer.php:1263
msgid "Header text color"
msgstr ""

#: inc/customizer.php:1280
msgid "Tagline text color"
msgstr ""

#: inc/customizer.php:1297
msgid "Outer background color"
msgstr ""

#: inc/customizer.php:1313
msgid "Content background color"
msgstr ""

#: inc/customizer.php:1329
msgid "Post title color"
msgstr ""

#: inc/customizer.php:1346
msgid "Post title hover color"
msgstr ""

#: inc/customizer.php:1364
msgid "Post links color"
msgstr ""

#: inc/customizer.php:1381
msgid "Post links hover color"
msgstr ""

#: inc/customizer.php:1397
msgid "Main border color"
msgstr ""

#: inc/customizer.php:1414
msgid "Widget title background color"
msgstr ""

#: inc/customizer.php:1431
msgid "Widget title text color"
msgstr ""

#: inc/customizer.php:1449
msgid "\"View Post\" button background"
msgstr ""

#: inc/customizer.php:1466
msgid "\"View Post\" button text"
msgstr ""

#: inc/customizer.php:1484
msgid "Social Icon color"
msgstr ""

#: inc/customizer.php:1500
msgid "Social Icon Hover color"
msgstr ""

#: inc/customizer.php:1517
msgid "Footer Bar Background Color"
msgstr ""

#: inc/customizer.php:1534
msgid "Footer Bar Text Color"
msgstr ""

#: inc/customizer.php:1555
msgid "Feature/Titles font:"
msgstr ""

#: inc/customizer.php:1588
msgid "Body font:"
msgstr ""

#: inc/customizer.php:1612
msgid "Header Text:"
msgstr ""

#: inc/customizer.php:1631
msgid "Main body:"
msgstr ""

#: inc/customizer.php:1650
msgid "Post titles:"
msgstr ""

#: inc/customizer.php:1688
msgid "Lowercase text"
msgstr ""

#: inc/customizer.php:1689
msgid ""
"This theme converts text such as post titles to uppercase (capital letters). "
"Select this option to disable this. This is useful if you change the fonts "
"in the theme."
msgstr ""

#: inc/fonts.php:28
msgid "Posts and Pages"
msgstr ""

#: inc/meta.php:12
msgid "Extra Post Options"
msgstr ""

#: inc/meta.php:18
msgid "Sidebar position"
msgstr ""

#: inc/meta.php:28
msgid "Post Location"
msgstr ""

#: inc/meta.php:29
msgid "This will be shown in the footer of the post and link to a Google Map."
msgstr ""

#: inc/meta.php:31
msgid "e.g. London"
msgstr ""

#: inc/meta.php:35
msgid "Extra Post Excerpt (\"Excerpt/Summary\" Post Layout only)"
msgstr ""

#: inc/meta.php:41
msgid "Disclaimer to display in footer"
msgstr ""

#: inc/template-tags.php:48 inc/template-tags.php:51
msgid "Previous Post"
msgstr ""

#: inc/template-tags.php:64 inc/template-tags.php:66
msgid "Next Post"
msgstr ""

#: inc/template-tags.php:97 inc/template-tags.php:119
msgid "Edit"
msgstr ""

#: inc/template-tags.php:111
msgid "Your comment is awaiting moderation."
msgstr ""

#: searchform.php:3
msgid "Type some keywords..."
msgstr ""

#~ msgid "Header icon color"
#~ msgstr "Header icon colour"

#~ msgid "Widget title border color"
#~ msgstr "Widget title border colour"

#~ msgid ""
#~ "Remember, you can also change the appearance of your site by using the "
#~ "Customizer screen."
#~ msgstr ""
#~ "Remember, you can also change the appearance of your site by using the "
#~ "Customiser screen."

#~ msgid ""
#~ "Widget setup not complete. You must add your bloglovin profile to the "
#~ "'Social Media Icons' section of the <a href='%s'>Customizer</a>."
#~ msgstr ""
#~ "Widget setup not complete. You must add your bloglovin profile to the "
#~ "'Social Media Icons' section of the <a href='%s'>Customiser</a>."

#~ msgid ""
#~ "You can style this widget in the <em>Bloglovin' Widget</em> section of "
#~ "the <a href='%s'>Customizer</a>."
#~ msgstr ""
#~ "You can style this widget in the <em>Bloglovin' Widget</em> section of "
#~ "the <a href='%s'>Customiser</a>."

#~ msgid ""
#~ "We have selected some of our favorite fonts from Google Fonts.  If you "
#~ "would like even more font options, we recommend using <a href=\"https://"
#~ "wordpress.org/plugins/easy-google-fonts/\" target=\"_blank\" rel="
#~ "\"nofollow\">this plugin</a>."
#~ msgstr ""
#~ "We have selected some of our favourite fonts from Google Fonts.  If you "
#~ "would like even more font options, we recommend using <a href=\"https://"
#~ "wordpress.org/plugins/easy-google-fonts/\" target=\"_blank\" rel="
#~ "\"nofollow\">this plugin</a>."
