��          �   %   �      0     1     B     O     ]     v     �     �     �     �     �     �          %     6     M     ^     u     �  
   �  �   �     G     e  \   }  _  �     :     L     Z     i     �     �     �     �     �     �     	     "     :     L     d     v     �     �     �  �   �     e	     �	  \   �	                                                       
                                 	                                       Background color Border color Color Options Content background color Footer Bar Background Color Footer Bar Text Color Header text color Main border color Navbar background color Navbar text color Navbar text hover color Outer background color Post links color Post links hover color Post title color Post title hover color Social Icon Hover color Social Icon color Text color Use these options to add a background image to your site. Note, if you would like a solid color please use the options in the "Color Options" section above. Widget title background color Widget title text color You will need to enable the sidebar in the Customizer before adding widgets to this section. Project-Id-Version: pipdig Theme
POT-Creation-Date: 2018-03-06 17:41+0000
PO-Revision-Date: 2018-03-06 17:41+0000
Last-Translator: pipdig <hello@pipdig.co>
Language-Team: pipdig
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: css
X-Poedit-SearchPathExcluded-2: js
X-Poedit-SearchPathExcluded-3: inc/plugins
 Background colour Border colour Colour Options Content background colour Footer Bar Background Colour Footer Bar Text Colour Header text colour Main border colour Navbar background colour Navbar text colour Navbar text hover colour Outer background colour Post links colour Post links hover colour Post title colour Post title hover colour Social Icon Hover colour Social Icon colour Text colour Use these options to add a background image to your site. Note, if you would like a solid colour please use the options in the "Colour Options" section above. Widget title background colour Widget title text colour You will need to enable the sidebar in the Customiser before adding widgets to this section. 