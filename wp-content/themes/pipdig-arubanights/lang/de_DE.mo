��    %      D  5   l      @     A     R     `     n     w     |     �     �     �     �     �     �     �  =   �     '     6     B  	   Q     [     i     u  	   �     �     �     �  F   �                 \   :     �  �   �  �   J  	   �     �  \     �  i     �	     
     
  
   "
  
   -
     8
     @
  	   Q
  	   [
     e
     z
     �
     �
  [   �
               .     ?     Q     a     s  	   �     �     �  
   �  R   �            5   *  n   `     �  �   �  �   �     �     �  i   �                "                   !                                                                         %              #   	         $                              
               Background Image Click to view Color Options Comments Edit Follow: Font Options Footer Footer Column Footer Full Width General Options Header Full Width Header Options It looks like this page is missing. Maybe try a search below? Layout Options Newer Posts Newer projects Next Post Nothing Found Older Posts Older projects Portfolio Post Layout Options Previous Post Primary Menu Ready to publish your first post? <a href="%1$s">Get started here</a>. Search button Sidebar Sorry, We can't find that page! Sorry, but nothing matched your search terms. Please try again with some different keywords. Type some keywords... Use these options to add a background image to your site. Note, if you would like a solid color please use the options in the "Color Options" section above. Use these settings to select different post layouts for your blog. You can also change the number of posts displayed by going to 'Settings > Reading' in your dashboard. View Post Where to next? You will need to enable the sidebar in the Customizer before adding widgets to this section. Project-Id-Version: pipdig Theme
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-01 18:41+0100
PO-Revision-Date: 2018-10-01 18:41+0100
Last-Translator: Tina Gallinaro <tinagallinaro@googlemail.com>
Language-Team: German
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: css
X-Poedit-SearchPathExcluded-2: js
X-Poedit-SearchPathExcluded-3: inc/plugins
 Hintergrundbild Klicke zum Zeigen Farboptionen Kommentare Bearbeiten Folgen: Schrift-Optionen Fußzeile Fußzeile Vollbreite Fußzeile Allgemeine Optionen Vollbreiter Header Header Optionen Es sieht so aus, als würde diese Seite fehlen. Vielleicht versuchen Sie eine Sucheanfrage? Layout Optionen Neuere Beiträge Neuere Beiträge Nächster Beitrag Nichts gefunden Ältere Beiträge Ältere Beiträge Portfolio Betragslayout Optionen Vorheriger Beitrag Hauptmenü Bereit Deinen ersten Post zu veröffentlichen? <a href="%1$s"> Hier loslegen </a>. Suche Seitenleiste Entschuldigung, wir können diese Seite nicht finden! Sorry, aber nichts entspricht Ihren Suchbegriffen. Bitte versuchen Sie es erneut mit einigen anderen Keywords. Suche Verwenden Sie diese Optionen, um ein Hintergrundbild zu Ihrer Website hinzuzufügen. Beachten Sie, wenn Sie eine einfarbige Farbe wünschen, verwenden Sie bitte die Optionen im Abschnitt "Farboptionen" oben. Verwenden Sie diese Einstellungen, um verschiedene Post-Layouts für Ihr Blog auszuwählen. Sie können auch die Anzahl der angezeigten Beiträge ändern, indem Sie in Ihrem Dashboard auf 'Einstellungen> Lesen' klicken. Beitrag ansehen Wohin als nächstes? Sie müssen die Seitenleiste im Customizer aktivieren, bevor Sie Widgets zu diesem Abschnitt hinzufügen. 