��    +      t  ;   �      �     �     �     �     �     �     �               -     5     <  =   L     �     �     �     �  	   �     �     �     �     �               #  
   9     D     R     f     l     t     {  \   �  ,   �     %  �   ;  �   �  �   w  �   v  	   `	     j	     y	  $   �	  _  �	          "     9     B  
   N     Y     s     y     �     �     �  L   �     �     	               1     E     U     f  	   |     �     �     �     �     �     �                 +     b   >  6   �     �  �   �  �   �     ]     _  	   a     k     |  &   �                           !   %      
   	      "                    '                    $            (                                                )               #                +   *          &        Background Image Blog Post Options Close Me Color Options Comments Content background color Edit Extra Post Options Follow: Footer General Options It looks like this page is missing. Maybe try a search below? Layout Options Left Looking for Something? Newer Posts Next Post Nothing Found Older Posts Outer background color Page %s Popular Posts Post Categories: Post Category Options Posted in: Previous Post Product Categories: Right Search: Share: Sorry, We can't find that page! Sorry, but nothing matched your search terms. Please try again with some different keywords. These options will apply to post categories. Type some keywords... Use these options to add a background image to your site. Note, if you would like a solid color please use the options in the "Color Options" section above. Use these options to display a full width post slider under the header. NOTE: for a post to be included in the slider it needs to have a "Featured Image" set. Use this widget area to display an advertising banner below the main content of your website. Simply add a Text Widget by dragging it into this box, then copy and paste your banner advert code into it. You should use a responsive banner for this section. Use this widget area to display an advertising banner below your header. Simply add a Text Widget by dragging it into this box, then copy and paste your banner advert code into it. You should use a responsive banner for this section. View Post Where to next? You may also enjoy: Your comment is awaiting moderation. Project-Id-Version: pipdig Theme
POT-Creation-Date: 2016-06-28 17:49+0100
PO-Revision-Date: 2016-06-28 17:49+0100
Last-Translator: pipdig <hello@pipdig.co>
Language-Team: pipdig
Language: af_ZA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: css
X-Poedit-SearchPathExcluded-2: js
X-Poedit-SearchPathExcluded-3: inc/plugins
 Agtergrondprent Blog Inskrywing Opsies Maak Toe Kleuropsies Kommentaar Cor de fundo do conteúdo Wysig Ekstra Inskrywing Opsies Volg: Footer Algemene Opsies Dit lyk of hierdie bladsy vermis word. Probeer om daarna hier onder te soek. Uitlegopsies Links Op soek n iets? Nuwer Inskrywings Volgende Inskrywing Niks gevind nie Ouer Inskrywings Cor do fundo exterior Bladsy %s Artigos Populares Inskrywing Kategorieë: Inskrywing Kategorie Opsies Gepubliseer in: Vorige Inskrywing Produk Kategorieë Regs Soek: Deel: Jammer, die bladsy kan nie gevind word nie! Jammer, maar ons kry geen resultate vir jou soektog nie. Probeer asb weer met ander sleutelwoorde. Hierdie opsies sal van toepassing wees op kategorieë. Tik 'n paar sleutelwoorde... Gebruik hierdie opsies om agtergrondprente op jou webwerf te sit. Let wel, indien jy 'n soliede kleur wil hê, gebruik asseblief die opsies in die "Kleuropsies" seksie hierbo. Use estas opçções para adicional um Slider de largura total por baixo do cabeçalho. NOTA: Para que o artigo seja incluido no Slider terá que ter uma imagem de destaque associada.     Lees Meer Waarna volgende? Pode ainda gostar de: Jou kommentaar wag om gewysig te word. 