<?php
/**
 * Template Name: Full Width Page
 */
get_header(); ?>

	<div class="row">
		<div class="col-xs-12 content-area">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; ?>

		</div><!-- .content-area -->
	</div>

<?php get_footer(); ?>