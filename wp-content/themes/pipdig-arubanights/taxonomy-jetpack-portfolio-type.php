<?php get_header(); ?>

	<div class="row">
	
		<?php do_action('p3_main_row_start'); ?>
	
		<div class="<?php pipdig_left_or_right(); ?> content-area">
		
		<?php if (category_description()) { ?>
			<h1 class="entry-title p_post_titles_font"><?php echo single_cat_title(); ?></h1>
			<div class="cat_desk">
				<?php echo category_description(); ?>
			</div>
		<?php } ?>

		<?php if ( have_posts() ) { ?>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'p3_medium' );
				if ($thumb) {
					$bg = esc_url($thumb['0']);
				} else {
					$bg = pipdig_catch_that_image();
				}
				?>
				<div class="pipdig_portfolio_grid_item">
					<a href="<?php the_permalink(); ?>" class="p3_cover_me" style="background-image:url(<?php echo $bg; ?>);">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAAHgAQMAAACyyGUjAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAACxJREFUeNrtwTEBAAAAwiD7p7bGDmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkHVZAAAFam5MDAAAAAElFTkSuQmCC" alt="" class="p3_invisible" data-pin-nopin="true"/>
							<div class="pipdig_portfolio_grid_title_box">
							<h2 class="title"><?php the_title(); ?></h2>
							<div class="read_more"><?php _e('Click to view', 'p3'); ?></div>
							</div>
						</a>
					</div>
			<?php endwhile; ?>
					
			<div class="clearfix"></div>
			<div class="next-prev-hider"><?php pipdig_content_nav('nav-below'); ?></div>
			<?php pipdig_pagination(); ?>

		<?php } else { ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php } //endif ?>

		</div><!-- .content-area -->

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>
