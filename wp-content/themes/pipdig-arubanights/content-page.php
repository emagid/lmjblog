<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>

	<?php if (function_exists('rwmb_meta') && (rwmb_meta('pipdig_meta_hide_page_title') != true)) { ?>
		<header class="entry-header">
			<h1 class="entry-title p_post_titles_font"><?php the_title(); ?></h1>
		</header>
	<?php } ?>

	<div class="clearfix entry-content">
		<?php the_content(); ?>
	</div>
	
</article>

<?php if (function_exists('rwmb_meta') && (rwmb_meta('pipdig_meta_hide_page_header') == true)) { ?>
	<style>.site-header { height: 45px; opacity: 0; visibility: hidden; }</style>
<?php } ?>