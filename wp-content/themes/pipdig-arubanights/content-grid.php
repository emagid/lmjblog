<?php

$this_post_num = $wp_query->current_post;

$home_layout = absint(get_theme_mod('home_layout', 1));
$category_layout = absint(get_theme_mod('category_layout', 1));

if ( (is_home() && ($home_layout == 3 || $home_layout == 5)) || ((is_archive() || is_search()) && ($category_layout == 3 || $category_layout == 5)) ) { // offsets for kensington
	$this_post_num++;
} 

$post_class = $clearfix = '';
$grid_title = 'grid-title';

if (get_theme_mod('grid_excerpts')) {
	$post_class = ' col-sm-6';
	$grid_title = '';
	if ($this_post_num % 2 == 0) { // check if even or odd number
		$post_class = 'col-sm-6 grid_post_even';
	} else {
		$post_class = 'col-sm-6 grid_post_odd';
		$clearfix = '<div class="clearfix"></div>';
	}
} else {
	$post_class .= ' pipdig-grid-post';
	if ($this_post_num % 2 == 0) { // check if even or odd number
		$post_class = 'pipdig-grid-post pipdig-grid-post-even';
	} else {
		$post_class = 'pipdig-grid-post pipdig-grid-post-odd';
	}
}

$size = 'large';
if (pipdig_sidebar_check()) {
	$size = 'p3_medium';
}

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), $size );
if ($thumb) {
	$bg = esc_url($thumb['0']);
} else {
	$bg = pipdig_catch_that_image();
}

?>

<div class="nopin <?php echo $post_class; ?>">
<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
	<header class="entry-header">

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<span class="date-bar-white-bg">
			
				<span class="vcard author show-author">
					<span class="fn">
						<?php the_author_posts_link(); ?>
					</span>
					<span class="show-author"></span>
				</span>
				
				<?php if (!get_theme_mod('hide_dates')) { ?>
					<span class="entry-date updated">
						<time datetime="<?php echo get_the_date('Y-m'); ?>"><?php echo get_the_date(); ?></time>
					</span>
				<?php } ?>
				
				<?php if (get_theme_mod('show_main_cat')) { ?>
					<span class="main_cat"><?php echo pipdig_main_cat(); ?></span>
				<?php } ?>
				
			</span>
		</div>
		<?php endif; ?>
		<?php if (get_theme_mod('grid_excerpts')) { ?>
		<?php if (get_theme_mod('pipdig_lazy')) { ?>
			<a href="<?php the_permalink() ?>" class="p3_cover_me pipdig_lazy" data-src="<?php echo $bg; ?>">
		<?php } else { ?>
			<a href="<?php the_permalink() ?>" class="p3_cover_me" style="background-image:url(<?php echo $bg; ?>);">
		<?php } ?>
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAHCAQMAAAAtrT+LAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAENJREFUeNrtwYEAAAAAw6D7U19hANUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALIDsYoAAZ9qTLEAAAAASUVORK5CYII=" alt="<?php the_title_attribute(); ?>" class="p3_invisible" data-pin-nopin="true" data-p3-pin-img-src="<?php echo $bg; ?>" data-p3-pin-title="<?php the_title_attribute(); ?>" data-p3-pin-link="<?php the_permalink(); ?>"/>
		</a>
		<?php } else { ?>
		<h2 class="entry-title <?php echo $grid_title; ?> p_post_titles_font"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo pipdig_truncate(get_the_title(), 8); ?></a></h2>
		<?php } ?>
	</header>

	<div class="entry-summary">
		<?php if (get_theme_mod('grid_excerpts')) { ?>
		<h2 class="entry-title <?php echo $grid_title; ?> p_post_titles_font"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<?php } else { ?>
		<?php if (get_theme_mod('pipdig_lazy')) { ?>
			<a href="<?php the_permalink() ?>" class="p3_cover_me pipdig_lazy" data-src="<?php echo $bg; ?>">
		<?php } else { ?>
			<a href="<?php the_permalink() ?>" class="p3_cover_me" style="background-image:url(<?php echo $bg; ?>);">
		<?php } ?>
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAHCAQMAAAAtrT+LAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAENJREFUeNrtwYEAAAAAw6D7U19hANUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALIDsYoAAZ9qTLEAAAAASUVORK5CYII=" alt="<?php the_title_attribute(); ?>" class="p3_invisible" data-pin-nopin="true" data-p3-pin-img-src="<?php echo $bg; ?>" data-p3-pin-title="<?php the_title_attribute(); ?>" data-p3-pin-link="<?php the_permalink(); ?>"/>
		</a>
		<?php } ?>
		<?php
		if (get_theme_mod('grid_excerpts')) {
			echo '<span class="pipdig_grid_excerpt">'.pipdig_truncate(get_the_excerpt(), 24).'</span>';
		}
		?>
		<a class="more-link" href="<?php the_permalink(); ?>"><?php _e('View Post', 'pipdig-textdomain'); ?></a>
	</div>

	<footer class="entry-meta entry-footer">
		<?php if ( 'post' == get_post_type() ) { ?>

			<?php if(function_exists('pipdig_p3_social_shares')){ pipdig_p3_social_shares(); } ?>
			
			<?php if(!get_theme_mod('hide_comments_link')){ ?>
				<span class="commentz"><a href="<?php comments_link(); ?>" data-disqus-url="<?php echo esc_url(get_the_permalink()); ?>"><?php if (function_exists('pipdig_p3_comment_count')) { pipdig_p3_comment_count(); } else { ?>Comments<?php } ?></a></span>
			<?php } // end if ?>
			
		<?php } //end if ?>
	</footer>
	
	<?php if (function_exists('p3_schema_publisher')) p3_schema_publisher(); ?>
	
</article>
</div>

<?php echo $clearfix; ?>