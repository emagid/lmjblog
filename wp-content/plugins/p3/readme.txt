=== pipdig Power pack (p3) ===
Contributors: pipdig
Requires at least: 4.0
Tested up to: 4.9
Stable tag: master
License: Copyright 2018 pipdig Ltd. All rights reserved.

The core functions of any pipdig theme.

== Description ==

Please check the changelog before updating :)

== Changelog ==

= 4.1.5 (05 November 2018) =
* PHP 7.2 compatibility.

= 4.1.4 (31 October 2018) =
* Update Slovenian language.

= 4.1.3 (14 October 2018) =
* Minor change to the [Pinterest Hover Button](https://support.pipdig.co/articles/wordpress-custom-pinterest-hover-button/) to fix a bug in Gutenberg.

= 4.1.1 (26 September 2018) =
* New option to include a "read More" button in the [Profile Photo widget](https://i.imgur.com/mHoNA3b.png).

= 4.0.0 (21 September 2018) =
* Update some features in line with newest version of [Gutenberg](https://www.pipdig.co/blog/gutenberg/).
* New option to show/hide widgets on the mobile layout - [read more](https://support.pipdig.co/articles/wordpress-hide-widget-on-mobile/).
* New option to use the new version of Font Awesome (v5).
* Add new "shop" link under the "pipdig > Social Links" page. Can be used to add a link to any shop page you like, e.g. [Shopr](https://www.pipdig.co/shop/shopr-wordpress-plugin/) or [Liketoknow.it](https://www.liketoknow.it/).
* More accurate Popular Posts widget stats from Jetpack.
* Increase max limit on [Featured Categories](https://support.pipdig.co/articles/wordpress-featured-categories/) to 6.
* Increase max limit on random posts widget to 8.
* Increase max limit on popular posts widget to 8.

= 3.14.0 (12 September 2018) =
* Display website link in [author bio](https://support.pipdig.co/articles/wordpress-author-bio-box/).
* Option to display the post location tag in the [Featured Categories Shortcode](https://support.pipdig.co/articles/wordpress-featured-categories-shortcode/).

= 3.13.8 (10 August 2018) =
* Fix an issue with the 'Large Rectangle Slider' which was not showing in some rare cases.

= 3.13.7 (09 July 2018) =
* Link post titles to the post in the "Popular Posts" widget.
* Fix an incompatibility with the Jetpack slideshow shortcode.

= 3.13.5 (03 July 2018) =
* Some minor improvements to image loading which should increase page load times.
* Argentinian Spanish added to languages.

= 3.13.4 (29 June 2018) =
* [New option](https://i.imgur.com/fmUB9Ih.png) to change the size of the [social icons in the navbar](https://support.pipdig.co/articles/wordpress-how-to-add-social-icons-to-the-navbar/).

= 3.13.3 (25 June 2018) =
* [Author bio box](https://support.pipdig.co/articles/wordpress-author-bio-box/) now displays social links from Yoast SEO.

= 3.13.1 (15 June 2018) =
* New [color options](https://i.imgur.com/oBl3ESW.gif) in the [Instagram section](https://support.pipdig.co/articles/wordpress-how-to-display-an-instagram-feed/) of the Customizer.

= 3.12.3 (07 June 2018) =
* New [horizontal layout](https://i.imgur.com/N6HPt14.png) option in the [Profile Photo](https://support.pipdig.co/articles/wordpress-profile-photo-widget/).

= 3.12.1 (30 May 2018) =
* Email subscribe widget privacy policy text is now translatable.

= 3.12.0 (27 May 2018) =
* New [horizontal layout](https://i.imgur.com/VsLhGXU.png) option in the [Popular Posts widget](https://support.pipdig.co/articles/wordpress-popular-posts-widget/).

= 3.11.2 (22 May 2018) =
* Select an image for categories that are shown in the [Featured Categories](https://support.pipdig.co/articles/wordpress-featured-categories/) option.
* Display an "Author" tag next to commenter's name if they are the author of that blog post.

= 3.11.0 (16 May 2018) =
* Improved "show likes on hover" styling on Instagram images.
* Higher quality video thumbnails from YouTube.
* Higher quality images from Pinterest.

= 3.10.1 (08 May 2018) =
* Improve loading time for images in the Media library. This may take a few weeks to start working fully.

= 3.10.0 (03 May 2018) =
* New! Shortcode to display Google Adsense ads [within post content](https://support.pipdig.co/articles/wordpress-display-adsense-start-end-blog-posts/#article_display_in_content).

= 3.9.0 (30 April 2018) =
* New! [Spotify widget](https://support.pipdig.co/articles/wordpress-spotify-widget/).
* Pinterest Hover Button can now use data-pin-description attribute.

= 3.8.3 (16 April 2018) =
* Site speed improvements by changing Schema tags to JSON-LD.

= 3.8.2 (9 April 2018) =
* The Popular Posts widget now uses data from Jetpack Stats.
* Option to change the number of posts in the 'Large Rectangle Slider'.

= 3.7.9 (13 March 2018) =
* Option to add up to 5 categories via the [Featured Categories](https://support.pipdig.co/articles/wordpress-featured-categories/).

= 3.7.8 (27 February 2018) =
* Display [Profile Photo Widget](https://support.pipdig.co/articles/wordpress-profile-photo-widget/) horizontally if not in sidebar or footer column.

= 3.7.7 =
* Display number of products in cart when using WooCommerce.

= 3.7.5 =
* Fix Pinterest hover button did not work on some images.
* Minor speed improvement by combining CSS files.

= 3.7.4 =
* Clear Instagram cache when plugin is deactivated.
* Update German translations.

= 3.7.2 =
* New: Option to link to the post category in [Featured Categories](https://support.pipdig.co/articles/wordpress-featured-categories/).

= 3.7.1 =
* Fix typo in [Google Adsense widget](https://support.pipdig.co/articles/wordpress-google-adsense-widget/) options.

= 3.7.0 =
* New: [Google Adsense widget](https://support.pipdig.co/articles/wordpress-google-adsense-widget/).

= 3.6.6 =
* Enable Pinterest Hover Button on mobile by default.

= 3.6.4 =
* Option to exclude posts from a category in the [Related Posts](https://i.imgur.com/uoABhQD.png) feature.

= 3.6.3 =
* Increase maximum columns in the Pinterest and Instagram widget from 4 to 5.
* Don't add [Pinterest Hover Button](https://support.pipdig.co/articles/wordpress-custom-pinterest-hover-button/) to images which are smaller than 400px wide.
* Move custom code from Hooks to last position in footer.

= 3.6.0 =
* New: Add Goodreads to social links (Not the official icon yet whilst we're waiting for this to become available).
* New: Display an [Author Bio](https://support.pipdig.co/articles/wordpress-author-bio-box/) at the end of blog posts.

= 3.5.1 =
* Option to display [Pinterest Hover Button](https://support.pipdig.co/articles/wordpress-custom-pinterest-hover-button/) on mobile as well as desktop.

= 3.5.0 =
* "Related Posts" can now be selected using categories or tags. Find this new option in the "Related Posts" section of the Customizer.

= 3.4.0 =
* Automatically play YouTube videos when clicked.
* Include Google+ in social footer links/counters.

= 3.3.0 =
* Option to change the size of images in the [pipdig_banner shortcode](https://wp.me/p79IlF-ij).

= 3.2.2 =
* New option - set header image to full width of the screen - [https://i.imgur.com/qzt6joG.png](https://i.imgur.com/qzt6joG.png).
* Improve loading time for CSS (minify and serve via CDN).
* Fix issue with YouTube video margins.

= 3.1.0 =
* Add Reddit and Digg to the "Social Links" page. Display icons in main menu if available.

= 3.0 =
* Option to change size of the icons in the [Social Sidebar](https://support.pipdig.co/articles/wordpress-social-sidebar/).
* Add "Houzz" to social links.
* Add [visibility options](https://support.pipdig.co/articles/wordpress-show-hide-widgets-different-pages/) for widgets.
* Option to resize image used in the [Image Widget](https://support.pipdig.co/articles/wordpress-image-widget/).
* New social stats page at 'pipdig > Social Stats'.